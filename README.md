# OpenAnolis社区

## 介绍

代码仓 Community 保存了关于 OpenAnolis 社区的所有信息，包括社区治理、开发者贡献指南、沟通交流指南等内容。



## 联系方式

你可以通过以下几个方式与我们取得联系

【方式一】Email ：contact@openanolis.cn

【方式二】加入钉钉群（扫码或搜索群号：33311793）

![img](https://oss.openanolis.cn/fragment/jmcwpqwcsgdddhlzgarz)

您也可以关注 OpenAnolis 社区微信公号，获取更多社区信息

![img](https://oss.openanolis.cn/fragment/plohryzhfyholejfbgoa)

