## SIG主页

https://openanolis.cn/sig/AI_SIG

## SIG目标
AI SIG致力于构建Anolis OS上AI软件栈，包括流行AI框架TensorFlow/PyTorch等，AI加速库，和相应软件方案。


## 使用教程
在 Anolis OS 8.x 系统上

```
yum update -y https://mirrors.openanolis.cn/anolis/8.6/BaseOS/x86_64/os/Packages/anolis-release-8.6-1.an8.x86_64.rpm https://mirrors.openanolis.cn/anolis/8.6/BaseOS/x86_64/os/Packages/anolis-gpg-keys-8.6-1.an8.noarch.rpm https://mirrors.openanolis.cn/anolis/8.6/BaseOS/x86_64/os/Packages/anolis-repos-8.6-1.an8.x86_64.rpm

yum install -y https://mirrors.aliyun.com/epel/epel-release-latest-8.noarch.rpm

cat > /etc/yum.repos.d/epao.repo << EOF
[epao]
name=epao
baseurl=https://mirrors.openanolis.cn/epao/8/x86_64/
enabled=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-ANOLIS
gpgcheck=1
EOF

yum install -y tensorflow
yum install -y pytorch 
yum install -y intel-extension-for-pytorch

```


## 成员列表
| 成员  |  角色 |
| ------------ | ------------ |
| forrest_ly |  maintainer | 
| wenhuanhuang  | maintainer | 


## SIG仓库

Source code repositories:
- https://gitee.com/src-anolis-ai/pytorch
- https://gitee.com/src-anolis-ai/intel-extension-for-pytorch
- https://gitee.com/src-anolis-sig/tensorflow
- https://gitee.com/src-anolis-sig/neural-compressor
