## SIG Home

https://openanolis.cn/sig/qa

## SIG Mission

In openAnolis community, quality assurance enables the delivery of the highest possible quality of AnolisOS distributions.

## Meetings
Biweekly, Wednesday, 10:00 AM

## Chat GROUP

![](assets/dingding_group_qa.png)
