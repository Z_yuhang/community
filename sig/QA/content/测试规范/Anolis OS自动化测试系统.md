# Anolis OS自动化测试系统

## 龙蜥社区测试架构
以社区版T-One测试平台为中心，构建龙蜥社区测试服务：

![Anolis OS自动化测试01](assets/AnolisTestPic21.png)

## Anolis OS自动化测试体系

![Anolis OS自动化测试02](assets/AnolisTestPic22.png)

Anolis OS社区版的测试都会基于T-One测试平台来进行，其中测试用例在gitee代码仓库中管理，会合入来自研发、测试、社区贡献者提交的测试用例，由T-One测试平台统一运行, 自动化测试流程有几种场景：
- 流程一：研发同学或者社区用户提交产品代码到codeup产品代码仓库，会通过预设的hook触发CI测试任务, T-One测试平台执行CI测试用例，回传CI测试结果到PR页面；
- 流程二：研发/测试/社区用户提交测试代码到gitee测试代码仓库，定时任务在预设的时间触发Nightly Test测试任务，T-One测试平台会拉取最新测试代码，分配测试机器，构建kernel，安装kernel（安装package），运行测试，上传测试结果;
- 流程三：产品发布阶段，PM拉通研发/测试，通告Code Freeze时间点，开始版本发布测试。研发同学通过构建系统出RC内核或ISO镜像，测试同学开始版本测试。版本测试阶段，测试同学使用T-One测试平台进行自动化测试，同样会拉取最新测试代码，进行Kernel/Packge/Image功能回归测试。
