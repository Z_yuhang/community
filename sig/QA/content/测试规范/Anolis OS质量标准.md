# Anolis OS版本质量标准
## Anolis OS研发代码合入标准

- 内核修改必须通过代码门禁检查，且需有单元测试用例覆盖
- 软件包修改需通过代码门禁检查，abi兼容性问题需经过评估

## Anolis OS版本发布质量标准

- S1、S2、P1、P2为必解问题
- S3，P3问题修复率要求超过50%
- S4, S5, P5类问题尽可能修复

Anolis OS缺陷严重性及优先级定义见**附录A**。

## Anolis OS缺陷管理规范
Anolis OS缺陷提交流程与管理规范，

- Bug需统一提交龙蜥社区[Bug管理系统](https://bugs.openanolis.cn/my_view_page.php)
- Bug提交说明、流转规范、处理原则、备注规范，请参考[bug流程与规范](https://yuque.antfin-inc.com/poweros/team/rka9xw#NpnEE)

## 附录A：Anolis OS缺陷严重性及优先级
社区bugzilla系统缺陷严重程度和优先级列表：

| 严重性 | 说明 |
| --- | --- |
| S1 | 阻塞（Blocker） |
| S2 | 严重（Major） |
| S3 | 普通（Normal） |
| S4 | 轻微（Trivial） |
| S5 | 无影响（Enhancement） |

| 优先级 | 说明 |
| --- | --- |
| P1 | 紧急（Urgent） |
| P2 | 高（High） |
| P3 | 中（Medium） |
| P4 | 低（Low） |
