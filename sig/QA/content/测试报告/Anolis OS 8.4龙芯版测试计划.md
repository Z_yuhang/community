## 测试概述
针对Anolis OS 8.4龙芯版进行测试验证, 该版本的预期交付物为：

- ISO安装镜像：适用于物理机与虚拟机的OS系统安装镜像
- QCOW2虚拟机镜像：适用于虚拟机部署，作为初始镜像*

## 测试环境

- 镜像：Anolis OS 8.4龙芯版 ISO、Qcow2镜像
- 内核：anck-4.19内核
- 架构：loongarch
- 机型：LS3A5000、LS3C5000L

### 物理机环境
| 机器 | 架构 | 处理器 | 备注（BIOS、外设等） |
| --- | --- | --- | --- |
| LS3A5000桌面版 | loongarch64 | Loongson-3A5000 | UEFI：SMBIOS 3.2.0, DISK：ASMedia ASM1062 Serial ATA Controller, NIC：Realtek RTL8111/8168/8411 Gigabit Ethernet Controller |
| LS3C5000L服务器 | loongarch64 | Loongson-3C5000L |  |


### 虚拟机环境
| 宿主机 | 架构 | 配置 | 备注 |
| --- | --- | --- | --- |
| LS3A5000 | loongarch64 | 2c8g | Host OS：Anolis 8.4 龙芯版 |
| LS3C5000 | loongarch64 | 最大规格 | Host OS：Anolis 8.4 龙芯版 |


## 测试内容
| 类型 | 测试 | 描述 | RC1 | RC2 |
| --- | --- | --- | --- | --- |
| 功能测试 | 安装部署 | 包括不同硬件机型安装测试，以及安装过程中的功能点验证（时区、语言、分区、软件选择等） | chenjianglei | chenjianglei |
|  | 内核功能 | 系统调用、调度、内存、I/O、网络、IPC、Cgroups、Namespace、锁 | qingming，liuke |  |
|  | 基础功能 | 系统/用户/进程/内存/磁盘/文件/时间/网络/软件包管理、系统监控 | liuzhongying | qingming wulijuan |
|  | 系统服务 | NFS、TELNET、SSH、DHCP、DNS、NTP、HTTP、VNC等服务 | liuzhongying liuke | qingming wulijuan |
|  | 开发环境、语言支持 | 支持常见开发语言，例如C、C++、Java、Perl、Python、PHP、Golang、Ruby、Rust等 | liuzhongying | qingming wulijuan |
|  | 图形桌面 | 默认gnome桌面 | chenjianglei | chenjianglei |
|  | 虚拟化支持 | kvm虚拟化支持 | liusinan* liuke | qingming |
|  | 容器支持 | docker容器支持 | liusinan* liuke | qingming |
| 性能测试 | unixbench  lmbench  stream iozone  netperf | LS3A5000/3C5000L | desktop: chenjianglei server: liuzhongying liuke | lvzebin： desktop+server |
| 安全测试 | 系统安全测试 | 访问控制、日志审计、防火墙、加解秘算法等 | qingming | liuke |
| 兼容性测试 | 硬件兼容性 | SSD、NIC、RAID、GPU等外设支持 | all |  |
|  | 软件兼容性 | mysql、nginx、k8s、ceph等生态软件 | wulijuan | liuke |
| 稳定性测试 | 7x24小时压力测试 | 需覆盖物理机与虚拟机环境 | wulijuan |  |


## 测试计划
| 时间 | 任务 | 描述 |
| --- | --- | --- |
| 2021.12.10-2021.12.24 | Anolis OS 8.4龙芯版 ISO RC1版本测试 | 第一轮测试完整覆盖 |
| 2021.12.24-2022.1.7 实际：2021.12.31-2022.1.14 | Anolis OS 8.4龙芯版 ISO RC2版本测试 | 第二轮测试回归，问题验证 |
| 2022.1.21 | Anolis OS 8.4龙芯版测试报告输出 | 输出测试报告 |


## 测试结果
| 类型 | 测试用例 | 测试结果 |
| --- | --- | --- |
| 功能测试 | 安装部署 | 通过 |
|  | 内核功能 | 通过 |
|  | 基础功能 | 通过 |
|  | 系统服务 | 通过 |
|  | 开发环境、语言支持 | 通过 |
|  | 图形桌面 | 通过 |
|  | 虚拟化支持 | 通过 |
|  | 容器支持 | 通过 |
| 性能测试 | Micro benchmarks | 通过 |
| 安全测试 | 系统安全测试 | 通过 |
| 兼容性测试 | 硬件兼容性 | 通过 |
|  | 软件兼容性 | 通过 |
| 稳定性测试 | 7x24小时压力测试 | 通过 |
