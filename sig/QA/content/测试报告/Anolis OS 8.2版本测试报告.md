## 测试概述
Anolis OS 8.2做为第一个龙蜥操作系统发布版，将支持x86_64和aarch64两种硬件架构，适配Intel、AMD主流芯片平台之外，同时支持飞腾、鲲鹏、兆芯、海光等处理器平台，计划发布ISO安装镜像及VHD虚拟机镜像。Anolis OS 8.2版本测试计划将覆盖不同处理器硬件机型，以及不同架构虚拟化环境。

## 测试环境
**1，物理机测试环境**
| 序号 | 名称 | 架构 | CPU |
| --- | --- | --- | --- |
| 1 | Intel物理机 | x86_64 | Intel Cascad Lake |
| 2 | AMD物理机 | x86_64 | AMD Rome |
| 3 | 鲲鹏ARM物理机 | aarch64 | Kunpeng-920 |
| 4 | 飞腾ARM物理机 | aarch64 | Phytium FT2000PLUS |
| 5 | 海光物理机 | x86_64 | Hygon C86 7280 32-core Processor |
| 6 | 兆芯物理机 | x86_64 | ZHAOXIN Kaisheng KH-37800 |


**2，虚拟机测试环境**
| 序号 | 名称 | 备注 |
| --- | --- | --- |
| 1 | x86_64虚拟机1 | 宿主机Intel Xeon |
| 2 | x86_64虚拟机2 | 宿主机Hygon 7280 |
| 3 | aarch64虚拟机1 | 宿主机kunpeng920 |
| 4 | aarch64虚拟机2 | 宿主机FT2000PLUS |

## 测试内容
在Anolis OS 8.2 RC阶段的测试主要以验证系统安装部署、基础功能，以及系统稳定性为主：

- 系统基础功能测试
   - ISO安装测试
      - BIOS/UEFI
      - CD/USB/PXE
      - ANCK/RHCK
      - 最小安装/全量安装
      - Ext4/XFS
      - 传统分区/LVM分区
   - Kernel测试
      - 系统调用
      - 调度系统
      - IPC
      - 内存管理
      - 大页内存
      - 文件系统
      - AIO/DIO
      - Cgroups
      - Namespaces
   - Userspace测试
      - Yum包管理
      - Kdump机制
      - 基本系统服务
      - 基本系统命令
      - 系统监控/安全机制
      - 编程语言环境
      - 数据库/中间件/大数据/Web应用
      - 兼容性测试
   - 虚拟化测试
      - QEMU/KVM
      - Docker
      - Podman
   - 图形桌面测试
      - 系统管理
      - 浏览器
      - 多媒体
      - 中文支持
- 系统稳定性测试
   - 系统综合压力测试
   - CPU/调度系统稳定性测试
   - Memory内存压力测试
   - Filesystem及Block I/O压力测试
   - Network网络协议栈压力测试

## 测试分工
| 测试项 | 负责人 |
| --- | --- |
| ISO物理机安装部署及桌面测试 | 统信：diary-dq，gaomingyang，gaiyaning |
| Intel、鲲鹏物理机功能测试 |  |
| Intel、鲲鹏物理机稳定性测试 |  |
| KVM虚拟机安装部署及功能测试 | 阿里：shanxifanshi, ouyang25, anoliswanqian, wangpingping |
| Kernel/Userspace功能测试 |  |
| AMD及海光、兆芯、飞腾等硬件机型测试 |  |


## 测试周期

| 时间 | 描述 |
| --- | --- |
| 2020.1.1 ～ 2020.1.10 | 测试计划、测试资源协调 |
| 2020.1.11 ～ 2020.1.31 | 测试环境、测试用例准备 |
| 2020.2.1 ～ 2020.2.28 | Anolis OS 8.2 Alpha版本测试、问题修复及验证 |
| 2020.3.1 ～ 2020.3.31 | Anolis OS 8.2 Beta版本测试、问题修复及验证 |
| 2020.4.1 ～ 2020.4.15 | Anolis OS 8.2 RC1测试验证 |
| 2020.4.16 ～ 2020.4.30 | RC1版本问题修复及验证 |
| 2020.5.1 ～ 2020.5.10 | Anolis OS 8.2 RC2测试验证 |
| 2020.5.11 ～ 2020.5.15 | RC2版本问题修复及验证 |
| 2020.5.16 ～ 2020.5.16 | GA版本上线检查 |

## 测试结果
| 类型 | 测试项 | 测试结果 |
| --- | --- | --- |
| 系统基础功能测试 | ISO安装测试 | 通过 |
|  | Kernel测试 | 通过 | | 
|  | Userspace测试 | 通过 |
|  | 虚拟化测试 | 通过 |
|  | 图形桌面测试 | 通过 |
| 系统稳定性测试 | 系统综合压力测试 | 通过 |
|  | CPU/调度系统稳定性测试 | 通过 |
|  | Memory内存压力测试 | 通过 |
|  | Filesystem及Block I/O压力测试 | 通过 |
|  | Network网络协议栈压力测试 | 通过 |
