## 测试概述
AnolisOS 8.2 QU1版本在8.2GA版本基础上进行了内核及众多软件包更新，修复了多个已知问题，重新制作了x86_64和aarch64两种硬件架构的ISO系统安装镜像，需进行系统功能回归验证测试。

## 测试镜像
Note：以下镜像地址为测试镜像，仅供发布前测试使用，正式发布后请下载正式镜像

- x86_64
   - [http://build.openanolis.cn/kojifiles/repos/iso-box/x86_64/iso_box-anck-qu1/AnolisOS-8.2-QU1-x86_64-dvd.iso](http://build.openanolis.cn/kojifiles/repos/iso-box/x86_64/iso_box-anck-qu1/AnolisOS-8.2-QU1-x86_64-dvd.iso)
- aarch64
   - [http://build.openanolis.cn/kojifiles/repos/iso-box/aarch64/iso_box-anck-qu1/AnolisOS-8.2-QU1-aarch64-dvd.iso](http://build.openanolis.cn/kojifiles/repos/iso-box/aarch64/iso_box-anck-qu1/AnolisOS-8.2-QU1-aarch64-dvd.iso)

## 测试环境
| 环境 | 架构 | 备注 |
| --- | --- | --- |
| Intel物理机 | x86_64 | Intel Xeon CPU |
| 鲲鹏物理机 | aarch64 | Kunpeng-920 |

## 测试内容
| 测试项 | 测试要求 | 备注 |
| --- | --- | --- |
| 安装部署 | 系统安装部署 | 物理机安装、虚拟机安装、磁盘分区、逻辑卷、文件系统、ANCK/RHCK、最小安装、全量安装、图形桌面等。 |
| 基础功能 | 系统基础功能测试 | 包括系统服务、常用命令、软件包管理、系统软件、应用软件等基础功能测试。 |
| 特性验证 | 增量更新内容验证 | 针对Anolis OS 8.2 QU1版本更新内容进行相应的特性验证。 |
| 稳定性测试 | 72小时负载测试 | 系统长时间稳定性测试。 |

## 测试规划
| 时间 | 描述 |
| --- | --- |
| 2021.9.1～2021.9.2 | 准备测试镜像，申请测试资源 |
| 2021.9.3～2021.9.7 | 第一轮镜像测试，问题提交 |
| 2021.9.8～2021.9.9 | 第二轮镜像测试，问题验证 |

## 测试结果
| 镜像 | 测试项 | 负责人 | 状态 | 结果 |
| --- | --- | --- | --- | --- |
| AnolisOS 8.2 QU1 x86_64 | 安装部署 | diary-dq | 已完成 | 通过 |
|  | 基础功能 | anolislw | 已完成 | 通过 |
|  | 特性验证 | yinyongchen | 已完成 | 通过 |
|  | 稳定性测试 | kangwen429 | 已完成 | 通过 |
| AnolisOS 8.2 QU1 aarch64 | 安装部署 | diary-dq | 已完成 | 通过 |
|  | 基础功能 | anolislw | 已完成 | 通过 |
|  | 特性验证 | yinyongchen | 已完成 | 通过 |
|  | 稳定性测试 | kangwen429 | 已完成 | 通过 |

