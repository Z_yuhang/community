
## 测试概述
Anolis OS 8.2、8.4版本虚拟机镜像要发布到阿里云上， 需要进行测试验证，保证镜像质量。
## 测试镜像
镜像正式发布前需要以自定义镜像方式进行验证，镜像列表：

| OS Release | ARCH | Image | Note |
| --- | --- | --- | --- |
| **Anolis OS 8.2 ** | x86_64 | anolisos_8_2_x64_20G_rhck | kernel-4.18 |
|  |  | anolisos_8_2_x64_20G_anck | kernel-4.19 |
|  | arm64 | anolisos_8_2_arm64_20G_rhck | kernel-4.18 |
|  |  | anolisos_8_2_arm64_20G_anck | kernel-4.19 |
| **Anolis OS 8.4** | x86_64 | anolisos_8_4_x64_20G_rhck | kernel-4.18 |
|  |  | anolisos_8_4_x64_20G_anck | kernel-4.19 |
|  | arm64 | anolisos_8_4_arm64_20G_rhck | kernel-4.18 |
|  |  | anolisos_8_4_arm64_20G_anck | kernel-4.19 |


## 测试环境

**云环境**：

- 目前仅覆盖阿里云环境，其他云环境正在准备中。

**实例类型列表：**

| 云环境 | 架构 | 实例 | 说明 |
| --- | --- | --- | --- |
| 阿里云 | x86_64 | ecs.g5.large | SkyLake ECS小规格 |
|  |  | ecs.g5.16xlarge | SkyLake ECS大规格 |
|  |  | ecs.g6.large | Cascade Lake ECS小规格 |
|  |  | ecs.g6.26xlarge | Cascade Lake ECS大规格 |
|  |  | ecs.g6a.large | Rome ECS小规格 |
|  |  | ecs.g6a.32xlarge | Rome ECS大规格 |
|  |  | ecs.g7.large | IceLake ECS小规格 |
|  |  | ecs.g7.32xlarge | IceLake ECS大规格 |
|  |  | ecs.g7a.large | Milan ECS小规格 |
|  |  | ecs.g7a.32xlarge | Milan ECS大规格 |
|  |  | ecs.d1.2xlarge | 大数据实例，8 vCPU 32 GiB 4 * 5500 |
|  |  | ecs.d2s.5xlarge | 20 vCPU 88 GiB 8 * 7300 GiB |
|  |  | ecs.i1.xlarge | 本地ssd盘实例，4 vCPU 16 GiB 2 * 104 GiB |
|  |  | ecs.i2.xlarge | 4 vCPU 32 GiB 1 * 894 GiB |
|  |  | ecs.ebmg5s.24xlarge | SkyLake裸金属 |
|  |  | ecs.ebmg6.26xlarge | Cascade Lake裸金属 |
|  |  | ecs.ebmhfg7.48xlarge | Cooper Lake裸金属 |
|  | arm64 | ecs.g6r.large | ARM ECS小规格 |
|  |  | ecs.g6r.16xlarge | ARM ECS大规格 |


## 测试内容
| 测试项 | 测试要求 | 备注 |
| --- | --- | --- |
| 实例创建 | 实例创建测试 | 实例创建、登陆、管理、云盘、网络、监控等云主机适配测试。 |
| 基础功能 | 系统基础功能测试 | 包括系统服务、常用命令、软件包管理、系统软件、应用软件等基础功能测试。 |
| 磁盘测试 | 验证数据盘可用 | 磁盘分区、逻辑卷管理、文件系统格式化、文件系统挂载、以及Block I/O、文件I/O压力测试等。 |
| 稳定性测试 | 72小时负载测试 | 系统长时间稳定性测试。 |


## 测试规划
| 时间 | 描述 |
| --- | --- |
| 2021.8.11～2021.8.13 | 准备测试镜像，申请测试资源 |
| 2021.8.16～2021.8.30 | Anolis OS 8.2 VHD镜像第一轮测试 |
| 2021.9.7～2021.9.18 | Anolis OS 8.2 VHD镜像第二轮测试 |
| 2021.9.21～2021.9.30 | Anolis OS 8.4 VHD镜像第一轮测试 |
| 2021.10.12～2021.10.20 | Anolis OS 8.4 VHD镜像第二轮测试 |


## 测试结果
| 镜像 | 负责人 | 状态 | 结果 |
| --- | --- | --- | --- |
| anolisos_8_2_x64_20G_rhck | ouyang25 | 已完成 | 通过 |
| anolisos_8_2_x64_20G_anck | shanxifanshi | 已完成 | 通过 |
| anolisos_8_2_arm64_20G_rhck | yinyongchen | 已完成 | 通过 |
| anolisos_8_2_arm64_20G_anck | kangwen429 | 已完成 | 通过 |
| anolisos_8_4_x64_20G_rhck | wangjingjing | 已完成 | 通过 |
| anolisos_8_4_x64_20G_anck | Qinyj | 已完成 | 通过 |
| anolisos_8_4_arm64_20G_rhck | kangjiangbo | 已完成 | 通过 |
| anolisos_8_4_arm64_20G_anck | wangpingping | 已完成 | 通过 |

