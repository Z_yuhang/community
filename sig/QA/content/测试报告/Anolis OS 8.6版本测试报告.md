## 测试概述
针对Anolis OS 8.6版本进行测试验证, 该版本的预期交付物为：

- ISO安装镜像：适用于物理机与虚拟机的OS系统安装镜像（dvd、boot、minimal）
- QCOW2虚拟机镜像：适用于虚拟机部署，作为初始镜像
- Docker容器镜像：适用于容器安装部署

## 测试环境

### 软件环境
- 镜像：Anolis OS 8.6 ISO、Qcow2、Docker
- 内核：anck（4.19）、rhck（4.18）双内核
- 架构：x86_64和aarch64
- 机型：Intel，鲲鹏 ，海光，AMD，兆芯，飞腾

### 硬件环境
| 机器 | 架构 | 处理器 | 备注 |
| --- | --- | --- | --- |
| 禹信（Intel）服务器 | x86_64 | Intel(R) Xeon（R）Silver 4210 CPU@2.2GHz | 只有机器覆盖安装 |
| 清华同方PC机 | x86_64 | Intel(R) Core(TM) i7-10700 CPU @ 2.90GHz | UEFI：SMBIOS 3.2 |
| HP ProDesk680 | x86_64 | Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz | UEFI：SMBIOS 3.0.0 |
| 兆芯服务器 | x86_64 | ZHAOXIN KaiSheng KH-37800D@2.7GHz | UEFI：SMBIOS 3.1 |
| 飞腾服务器 | aarch64 | Phytium,S2500/64 | UEFI：SMBIOS 3.1.1 |
| 鲲鹏服务器 | aarch64 | Kunpeng-920 | |
| 海光服务器 | x86_64 | Hygon C86 7285 32-core Processor | |


## 测试内容
| 类型 | 测试项 | 描述 | 负责人 |
| --- | --- | --- | --- |
| 功能测试 | 安装部署 | 包括不同硬件机型安装测试，以及安装过程中的功能点验证（时区、语言、分区、软件选择等）；rhck、anck双内核验证；Full ISO、Boot ISO、Mini ISO验证等; 8.6特有修改：1）ANCK/RHCK分别作为引导内核测试, 2）识别机型选择是否支持双内核（海光  飞腾  兆芯看不到RHCK） | 张美/黄聪 |
|  | 基础功能 | 软件包测试、基础库、系统服务、基础命令等 | 黄聪 |
|  | 图形桌面 | 默认gnome桌面 | 张美 |
|  | 虚拟化支持 | kvm虚拟化支持 | 张美/黄聪 |
|  | 容器支持 | docker容器支持 | 黄聪 |
|  | kernel功能回归 | 内核基本功能回归测试 | 黄聪 |
| 性能测试 | unixbench lmbench stream iozone redis mysql | Intel（看协调时间）、海光和鲲鹏服务器 兆芯、飞腾 | 张美/黄聪 |
| 兼容性测试 | 硬件兼容性 | 1，Intel、鲲鹏 、海光、兆芯、飞腾等CPU 平台支持; 2，SSD、NIC、RAID、GPU等外设支持; 3、结果提交到社区 | 张美/黄聪 |
|  | 软件兼容性 | mysql、nginx、k8s、ceph等生态软件 | 张美/黄聪 |
| 系统迁移测试 | centos迁移到anolis8.6 | centos7、8 > anolis8.6 [https://openanolis.cn/sig/migration/doc/534424186337479143](https://openanolis.cn/sig/migration/doc/534424186337479143) 第6点 | 黄聪 |
| 系统升级测试 | anolis低版本升级到anolis8.6 | anolis8.2  > anolis8.6, anolis8.4  > anolis8.6 | 张美/黄聪 |
| 稳定性测试 | 7x24小时压力测试（最少3*24h） | 需覆盖x86_64和aarch64平台以及各硬件机型 （./testscripts/ltpstress.sh -p -S -n -t ） | 张美/黄聪 |


## 验收标准

- 镜像测试：所有支持的机型可以正常安装运行，虚拟机镜像可以正常部署
- 基础功能测试：不存在严重的功能问题
- 稳定性测试：7x24小时连续压力情况下，不存在系统宕机，压力测试结束后，系统工作正常

## 测试结果
### 功能测试
#### 安装测试
| **测试项** | **版本** | **测试子项** | **测试结果** |
| --- | --- | --- | --- |
| 语言设置 | anck/rhck | 中文 | 通过 |
|  | anck/rhck | 英文 | 通过 |
| 时区设置 | anck/rhck | Shanghai | 通过 |
| 软件源 | anck/rhck | 本地源 | 通过 |
|  | anck/rhck | 网络源 | N/A |
| 软件配置 | anck/rhck | server with GUI | 通过 |
|  | anck/rhck | server | 通过 |
|  | ank/rhck | Minimail Install | 通过 |
|  | anck/rhck | Workstation  | 通过 |
|  | anck/rhck | Custom Operation System | 通过 |
|  | anck/rhck | Virtualization Host | 通过 |
| 系统分区 | anck/rhck | 传统分区 | 通过 |
|  | anck/rhck | LVM分区 | 通过 |
| 磁盘设置 | anck/rhck | 磁盘加密 | 通过 |
| 文件系统 | anck/rhck | EXT4 | 通过 |
|  | anck/rhck | XFS | 通过 |
| 口令设置 | anck/rhck | Root口令设置 | 通过 |
| 用户设置 | anck/rhck | 增加普通用户 | 通过 |
| 系统登陆 | anck/rhck | 安装完成重启进入系统 | 通过 |

#### 系统基础功能测试

- kernel功能回归

  - ck26已经覆盖，测试通过

- smoke测试
   - x86_64: 测试通过
   - aarch64: 测试通过

- 软件包功能测试
   - qcow2镜像冒烟测试：
本次未使用自动化执行，手动执行结果：99/74/21/4(总计/成功/失败/跳过)，主要错误问题有compile部分需要源码下载的case fail（github问题）、dmesg中有calltrace打印（已提aone）等
[https://bugzilla.openanolis.cn/show_bug.cgi?id=1319](https://bugzilla.openanolis.cn/show_bug.cgi?id=1319)

- 虚拟化功能测试 
   - x86_64 
      - 兆芯机器验证完成: 测试通过
      - 海光机器验证完成: 测试通过
      - 清华同方机器验证完成: 测试通过
   - aarch64	
      - 飞腾机器验证完成: 测试通过


- 容器测试 
   - x86_64
      - 线下虚拟机验证通过
   - aarch64
      - 线下虚拟机验证通过

### 稳定性测试
#### x86_64稳定性
- 兆芯 KH-37800D
   - ltp-stress
      - anck :  运行时长:72h  
         - dvd iso: ltp  无异常
         - min iso: ltp  无异常

      - rhck:   运行时长:72h  
         - dvd iso: ltp 12h 无异常
         - min iso: ltp 12h 无异常

- 海光 Hygon C86 5185 16-core Processor
   - ltp-stress
      - anck: 运行时长：72
        - dvd iso: ltp  无异常

#### aarch64稳定性
- 飞腾 FT2500
   - ltp-stress 
      - anck :  运行时长:72h 
         - dvd iso: ltp  无异常
         -  min iso: ltp  无异常

- Kunpeng-920
   - ltp-stress 
      - anck :  运行时长:72h 
         - dvd iso: ltp  无异常

### 迁移测试
#### x86_64迁移 
- centos8.4 --> anolis8.6:  线上ecs迁移成功
- centos8.4--> anolis8.6:   清华同方迁移成功

####aarch64迁移
- centos8.4 --> anolis8.6: 线上ecs迁移成功
- centos8.4--> anolis8.6:  鲲鹏服务器迁移成功

### 兼容性测试
#### 硬件兼容性
测试覆盖完成，通过

#### 软件兼容性
测试覆盖完成，通过


## 遗留问题：
- [Anolis OS 8.6][x86_64][兆芯][anck/rhck] qcow2 dmesg error信息   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1342](https://bugzilla.openanolis.cn/show_bug.cgi?id=1342)
- [AnolisOS8.6][anck][x86_64][dubbo 兼容性] 启动服务端: [ERROR] 1 problem was encountered while building the effective   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1604](https://bugzilla.openanolis.cn/show_bug.cgi?id=1604)
- [AnolisOS8.6][anck][x86_64][PolarDB兼容性] 环境中yum源中无PolarDB   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1605](https://bugzilla.openanolis.cn/show_bug.cgi?id=1605)
- [Anolis8.6][anck][x86_64][httpd兼容性] httpd-2.4.17/httpd-2.4.18源码编译报错   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1620](https://bugzilla.openanolis.cn/show_bug.cgi?id=1620)
- [Anolis8.6][anck][x86_64][memcached兼容性] memcached-1.4.4源码编译报错   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1621](https://bugzilla.openanolis.cn/show_bug.cgi?id=1621)
- [AnolisOS8.6][anck][x86_64][oprofile兼容性] 环境中yum源中无oprofile   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1629](https://bugzilla.openanolis.cn/show_bug.cgi?id=1629)
- [AnolisOS8.6][anck][x86_64][Kubernetes兼容性] kubeadm init失败   
  [https://bugzilla.openanolis.cn/show_bug.cgi?id=1630](https://bugzilla.openanolis.cn/show_bug.cgi?id=1630)
