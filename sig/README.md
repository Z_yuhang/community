SIG 全称 Special Interest Group，针对特定的一个或多个技术主题而成立。社区按照不同 SIG 组织，以便更好管理和改善工作流程。SIG 成员主导 SIG 的治理、推动交付成果输出，并争取让交付成果成为社区发行的一部分。

欢迎任何人加入并参与贡献。您可以通过邮件列表与组内成员沟通，您在贡献的同时也将积累经验和提升影响力。

龙蜥社区 SIG 链接：https://openanolis.cn/sig

您可以在 SIG 主页审请创建 SIG，然后通过 PR 的形成提交到 community 仓库进行内容更新即可；社区官网 SIG 页面信息会与 community 仓库的 SIG 目录保持自动同步。

## SIG 目录规范
SIG 目录里定义了 SIG 自己的信息，这些信息会作为 SIG 的元信息作为社区基础设施和 SIG 同步程序的输入，以保证社区的自动化运作，所以在填写的时候需要遵守以下规范填写 SIG 信息。

1. 一个SIG目录里主要有三块内容：README.md、sig-info.yaml 和 content；README.md 的内容和 content 目录由用户自定义，具体请参考 [T-One SIG](https://gitee.com/anolis/community/tree/master/sig/T-One)。
2. sig-info.yaml 文件中定义了 name，description, mailing_list, meeting_url，repositories 以及 maintainers、contributors 等字段。
    - name： 需要与 SIG 目录名称保持一致。
    - en_name： 需要与 SIG 目录英文名称保持一致。
    - home_page：SIG路径，官网 SIG 主页的 URL (如：https://openanolis.cn/sig/t-one)。
    - description：SIG 的具体描述信息。
    - en_description：SIG 的英文具体描述信息。
    - mailing_list：SIG 的邮件列表。
    - meeting_url SIG 的固定会议记要信息，一般是 [etherpad](https://etherpad.openanolis.cn/) 的url。
    - maintainers / contributors：对应 SIG 的 maintainers 和 contributors。
        - name：maintainer / contributor 的人员显示名；非必填。
        - gitee_id：maintainer / contributor 的人员的 gitee 帐户 id；必填。
        - openanolis_id 会映射到社区帐户；必填。
        - organization：maintainer / contributor 的人员公司名；非必填。
    - repositories： 定义 SIG 相关的仓库信息。
        - 仓库的填写格式：{group_name}/{repo_name}，如 anolis/community
3. md 文件中图片的使用规范
   - 图片是一个地址：该地址必须为在官网 SIG 目录下上传图片后得到的图片地址，不支持其他图床上的地址。
   - 图片以相对路径的方式存放在本仓库下：需要在 md 文件的**同级目录**下创建一个名为 assets 的文件夹存放图片资源，并在 md 文件中使用 !\[](assets/图片名称) 语法引入图片。

## 同步逻辑说明
1. 同步程序主要会通过 sig-info.yaml 这个文件同步到社区SIG页面上（对应SIG基本信息）。
2. README.md 会作为 SIG 主页内容同步到社区官网对应的 SIG 主页。
3. content里内容的目录及内容跟SIG目录映射起来；同步程序会自动检测变更并同步到官网SIG。