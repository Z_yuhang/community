•小组名称

技术委员会运营小组

•小组介绍及目标

1. TC 日常运营
2. 社区技术项目和 SIG 辅导
3. 社区所有技术相关需求收集反馈

•项目列表及repo

 https://codeup.openanolis.org/codeup/community/tc

•路线图及工作计划

1. TC 例会的运营
2. 社区 SIG 的运营指导运营
3. 社区技术项目规划评审

•邮件列表及会议等沟通信息

请发邮件至 community@lists.openanolis.cn

•成员及维护人（使用网站账号ID）

运营小组成员构成为两部分：

1. 日常运营小组成员
2. 龙蜥技术委员