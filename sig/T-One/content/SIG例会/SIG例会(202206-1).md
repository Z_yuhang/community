## 会议主题
测试管理系统TestLib开发进度同步、演示、以及问题交流
## 会议时间
2022.06.01 11:00-12:00
## 参会人

- 参会方：来自电子五所(赛宝实验室)和阿里云的社区爱好者
- 参会人：yongchao、VosAmoWho、juexiao、wjn740、zhizhisheng、as461177513 等等
## 会议记要

1. 讨论了下阶段的开发需求和优先级
   1. TestLib要看到TetLib和T-one联动的效果；
   1. T-One在多种cpu平台、os平台的适配工作
      1. 硬件：龙芯，鲲鹏，华为麒麟、盘古等  
      1. OS：Anolis、统信、麒麟
      1. tone-cli适配 
      1. tone-agent适配 
   3. Testlib 业务功能完善 
      1. 用例导入 
      1. 权限体系完善 
      1. Testlib与T-One 平台的联动（基于任务与t-one对接）
   4. 测试方案维度的测试报告完善（性能结果、环境信息等
2. Testlib与T-One 平台的部署融合
2. 大家一起讨论了软硬件适配计划
   1. 五所后面会提供相关适配机器（龙芯，鲲鹏，麒麟、盘古终端）
   1. 重点是包括tone-agent和tone-cli的适配

