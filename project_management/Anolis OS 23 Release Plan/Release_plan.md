<table border=0 cellpadding=0 cellspacing=0 width=1098 style='border-collapse:
 collapse;table-layout:fixed;width:823pt'>
 <col width=180 style='mso-width-source:userset;mso-width-alt:5760;width:135pt'>
 <col width=127 style='mso-width-source:userset;mso-width-alt:4053;width:95pt'>
 <col width=165 style='mso-width-source:userset;mso-width-alt:5290;width:124pt'>
 <col width=119 style='mso-width-source:userset;mso-width-alt:3797;width:89pt'>
 <col width=155 style='mso-width-source:userset;mso-width-alt:4949;width:116pt'>
 <col width=352 style='mso-width-source:userset;mso-width-alt:11264;width:264pt'>
 <tr height=21 style='height:16.0pt'>
  <td colspan=6 height=21 class=xl70 width=1098 style='border-right:.5pt solid black;
  height:16.0pt;width:823pt'>Anolis OS 23 项目计划</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>事项</td>
  <td class=xl65 style='border-top:none;border-left:none'>开始时间</td>
  <td class=xl65 style='border-top:none;border-left:none'>截止时间</td>
  <td class=xl65 style='border-top:none;border-left:none'>时间段</td>
  <td class=xl65 style='border-top:none;border-left:none'>承接主体</td>
  <td class=xl65 style='border-top:none;border-left:none'>备注</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 style='height:16.0pt;border-top:none'>PoC 版本</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/1/1</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/3/31</td>
  <td class=xl68 style='border-top:none;border-left:none'>　</td>
  <td class=xl67 style='border-top:none;border-left:none'>阿里云、统信</td>
  <td class=xl67 style='border-top:none;border-left:none'>验证版本</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 style='height:16.0pt;border-top:none'>Preview 版本</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/4/1</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/10/31</td>
  <td class=xl68 style='border-top:none;border-left:none'>　</td>
  <td class=xl67 style='border-top:none;border-left:none'>阿里云、统信</td>
  <td class=xl67 style='border-top:none;border-left:none'>基础版本，提供开发验证</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>社区需求收集评审</td>
  <td class=xl66 style='border-top:none;border-left:none'>2022/9/1</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/6/30</td>
  <td class=xl66 style='border-top:none;border-left:none'>10月</td>
  <td class=xl65 style='border-top:none;border-left:none'>龙蜥社区TC/SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>需求需要上TC评审，确认承接主体和交付时间</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>社区需求开发交付</td>
  <td class=xl66 style='border-top:none;border-left:none'>2022/10/1</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/7/31</td>
  <td class=xl66 style='border-top:none;border-left:none'>10月</td>
  <td class=xl65 style='border-top:none;border-left:none'>龙蜥社区TC/SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>需求验收需要上TC评审</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>版本构建</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/8/1</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/8/13</td>
  <td class=xl66 style='border-top:none;border-left:none'>2周</td>
  <td class=xl65 style='border-top:none;border-left:none'>产品发布小队SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>版本分支代码冻结</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>版本全功能测试</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/8/14</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/9/10</td>
  <td class=xl66 style='border-top:none;border-left:none'>2周</td>
  <td class=xl65 style='border-top:none;border-left:none'>T-One测试平台SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>测试报告上TC评审，作为Release版本的预审</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>bug修复</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/9/11</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/9/24</td>
  <td class=xl66 style='border-top:none;border-left:none'>2周</td>
  <td class=xl65 style='border-top:none;border-left:none'>各SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>仅允许bug修复代码提交</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>回归测试1</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/9/25</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/8</td>
  <td class=xl66 style='border-top:none;border-left:none'>2周</td>
  <td class=xl65 style='border-top:none;border-left:none'>T-One测试平台SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>　</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>bug修复</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/9</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/22</td>
  <td class=xl66 style='border-top:none;border-left:none'>2周</td>
  <td class=xl65 style='border-top:none;border-left:none'>各SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>仅允许bug修复代码提交</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>回归测试2</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/23</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/30</td>
  <td class=xl66 style='border-top:none;border-left:none'>1周</td>
  <td class=xl65 style='border-top:none;border-left:none'>T-One测试平台SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'> </td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>Release版本发布评审</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/31</td>
  <td class=xl66 style='border-top:none;border-left:none'>2023/10/31</td>
  <td class=xl69 style='border-top:none;border-left:none'>1天</td>
  <td class=xl65 style='border-top:none;border-left:none'>龙蜥社区TC会议</td>
  <td class=xl65 style='border-top:none;border-left:none'>上TC评审后正式发布</td>
 </tr>
</table>

