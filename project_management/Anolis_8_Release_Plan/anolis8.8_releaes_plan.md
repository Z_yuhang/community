# 1 Anolis OS 8.8 项目规划

<table border=0 cellpadding=0 cellspacing=0 width=1098 style='border-collapse:
 collapse;table-layout:fixed;width:823pt'>
 <col width=180 style='mso-width-source:userset;mso-width-alt:5760;width:135pt'>
 <col width=127 style='mso-width-source:userset;mso-width-alt:4053;width:95pt'>
 <col width=165 style='mso-width-source:userset;mso-width-alt:5290;width:124pt'>
 <col width=119 style='mso-width-source:userset;mso-width-alt:3797;width:89pt'>
 <col width=155 style='mso-width-source:userset;mso-width-alt:4949;width:116pt'>
 <col width=352 style='mso-width-source:userset;mso-width-alt:11264;width:264pt'>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>里程碑</td>
  <td class=xl65 style='border-top:none;border-left:none'>开始时间</td>
  <td class=xl65 style='border-top:none;border-left:none'>截止时间</td>
  <td class=xl65 style='border-top:none;border-left:none'>承接主体</td>
  <td class=xl65 style='border-top:none;border-left:none'>备注</td>
 </tr>
<tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 style='height:16.0pt;border-top:none'>需求冻结</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/10/18</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/10/31</td>
  <td class=xl67 style='border-top:none;border-left:none'>产品发布 SIG（阿里云、统信） <br>龙芯开发 SIG（龙芯中科、中科方德、万里红、北京红旗、阿里云、统信）<br>Cloud Kernel SIG<br>龙蜥社区各 SIG </td>
  <td class=xl67 style='border-top:none;border-left:none'>5.10 内核性能摸底测试；<br>完成各 SIG 成果透出需求收集</td>
 </tr>
<tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 style='height:16.0pt;border-top:none'>内核测试</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/10/20</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/11/30</td>
  <td class=xl67 style='border-top:none;border-left:none'>产品发布 SIG（阿里云、统信）<br>Cloud Kernel SIG <br>QA SIG</td>
  <td class=xl67 style='border-top:none;border-left:none'>5.10 内核性能测试；<br>5.10 内核兼容性测试</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 style='height:16.0pt;border-top:none'>发布 Beta 版本</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/10/20</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/11/30</td>
  <td class=xl67 style='border-top:none;border-left:none'>产品发布 SIG（阿里云、统信）<br>龙芯开发 SIG（龙芯中科、中科方德、万里红、北京红旗、阿里云、统信）<br>龙蜥社区各 SIG </td>
  <td class=xl67 style='border-top:none;border-left:none'>龙芯同源异构（含 DDE）；<br> DDE 版本升级适配；<br> 龙蜥自研核心特性集成；<br>自研率统计工具；<br>兼容性规则和工具主体完成；<br>进入RC测试阶段</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl67 style='height:16.0pt;border-top:none'>发布 RC 版本</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/12/1</td>
  <td class=xl68 style='border-top:none;border-left:none'>2022/12/20</td>
  <td class=xl67 style='border-top:none;border-left:none'>产品发布 SIG（阿里云、统信）<br>龙芯开发 SIG（龙芯中科、中科方德、万里红、北京红旗、阿里云、统信）<br>龙蜥社区各 SIG <br>QA SIG</td>
  <td class=xl67 style='border-top:none;border-left:none'>版本基线升级调整；<br>验证版本；<br>版本问题回归</td>
 </tr>
 <tr height=21 style='height:16.0pt'>
  <td height=21 class=xl65 style='height:16.0pt;border-top:none'>发布 GA 版本</td>
  <td class=xl66 style='border-top:none;border-left:none'>2022/12/20</td>
  <td class=xl66 style='border-top:none;border-left:none'>2022/12/30</td>
  <td class=xl67 style='border-top:none;border-left:none'>龙蜥社区TC SIG <br>产品发布 SIG（阿里云、统信）<br>龙芯开发 SIG（龙芯中科、中科方德、万里红、北京红旗、阿里云、统信）<br>龙蜥社区各 SIG <br>QA SIG</td>
  <td class=xl65 style='border-top:none;border-left:none'>版本问题回归；<br>TC 发布评审</td>
</table>

# 2 版本常见问题集锦
## 2.1 自研的调整是否会造成与 Anolis OS 8 之前版本的兼容性问题？
回答：**不会的**！自研版本演进的过程中，会根据客户实际业务场景，对涉及到的包按照龙蜥社区分层分类的理论进行区分，龙蜥社区的各理事单位和合作伙伴开发者会依据各自业务定位和需求，在L0-L3 层选择自己的研发重点；
同时社区会形成兼容性理论和保障， 来保障 Anolis OS 8 版本在 API 和 ABI 演进过程中的向后兼容性。最终的目的是核心仓库兼容性在单一大版本升级过程中是零影响的。应用流仓库的兼容性在单一大版本升级过程中完全可以通过模块（Module）机制来保障。

## 2.2 自研特性增多后，是否还可以保障安全更新的及时修复？
回答：**一定的**！社区目前已经形成了一套完整的漏洞感知机制， 会针对严重的漏洞及时感知；针对感知到的漏洞，龙蜥社区会按照 SLO 原则来保障（Critical 问题修复时间 24 小时、Important 问题的修复时间 48小时）。


## 2.3 版本自研特性增多后，对龙蜥社区下游发行版的认证流程有什么影响呢？
回答：龙蜥社区已经提供覆盖多场景的下游版本认证流程和对应的短期和长期（长期方案还在开发中）的构建通用方案。来支持和保障下游版本企业客户可完成下游版本制作的流程的自引导，最终实现龙蜥社区的高效的服务构建体系；认证流程和通用方案会引导社区重点技术（如云原生方案）的对外合作输出，保障关键场景的统一兼容性。
