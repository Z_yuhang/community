| 软件包名                       | 软件包层级 | 当前版本             | 是否做过重新选型 | 负责人          | 是否自研或有自研补丁 | 备注               |
| :----------------------------- | :--------- | :------------------- | :--------------- | :-------------- | :------------------- | :----------------- |
| kernel                         | 0          | 5.10.134             |                  |                 |                      |                    |
| anolis-release                 | 1          | 23                   | 是               | Meredith        |                      |                    |
| basesystem                     | 1          | 23                   | 是               | Meredith        |                      |                    |
| binutils                       | 1          | 2.38                 |                  |                 |                      |                    |
| dbus                           | 1          | 1.13.22              |                  |                 |                      |                    |
| dbus-broker                    | 1          | 32                   |                  |                 |                      |                    |
| dnf                            | 1          | 4.14.0               | 是               | fe_zhang        |                      |                    |
| filesystem                     | 1          | 3.16                 | 是               | Meredith        |                      |                    |
| gcc                            | 1          | 12.1.0               |                  |                 |                      |                    |
| gdb                            | 1          | 11.2                 | 是               | su-feng-nmg     |                      |                    |
| glibc                          | 1          | 2.36                 |                  |                 |                      |                    |
| grub2                          | 1          | 2.06                 |                  | fe_zhang        |                      |                    |
| linux-firmware                 | 1          | 20220209             |                  | xuchun-shang    |                      |                    |
| openssl                        | 1          | 3.0.7                |                  |                 |                      |                    |
| rpm                            | 1          | 4.18.0               |                  | chen_yu_ao      |                      |                    |
| setup                          | 1          | 2.14.2               |                  |                 |                      |                    |
| systemd                        | 1          | 250.4                |                  |                 |                      |                    |
| system-rpm-config              | 1          | 23                   | 是               | Meredith        |                      |                    |
| util-linux                     | 1          | 2.38~rc4             |                  | chen_yu_ao      |                      |                    |
| acl                            | 2          | 2.3.1                |                  |                 |                      |                    |
| anaconda                       | 2          | 37.5                 |                  |                 |                      |                    |
| at                             | 2          | 3.2.5                |                  | su-feng-nmg     |                      |                    |
| attr                           | 2          | 2.5.1                | 是               | chen_yu_ao      |                      |                    |
| audit                          | 2          | 3.0.7                |                  |                 |                      |                    |
| authselect                     | 2          | 1.4.0                |                  |                 |                      |                    |
| bash                           | 2          | 5.2~alpha            |                  |                 |                      |                    |
| bcc                            | 2          | 0.24.0               |                  |                 |                      |                    |
| bind                           | 2          | 9.18.6               |                  |                 |                      |                    |
| biosdevname                    | 2          | 0.7.3                | 是               | kun-llfl        |                      |                    |
| bpftrace                       | 2          | 0.14.1               | 是               | xuchun-shang    |                      |                    |
| checkpolicy                    | 2          | 3.3                  |                  |                 |                      |                    |
| chkconfig                      | 2          | 1.20                 |                  |                 |                      |                    |
| chrony                         | 2          | 4.3                  | 是               | dian-ws         |                      |                    |
| clang                          | 2          | 13.0.1               |                  |                 |                      |                    |
| coreutils                      | 2          | 9.1                  |                  | carlo_bai       |                      |                    |
| cpio                           | 2          | 2.13                 |                  | xuchun-shang    |                      |                    |
| cracklib                       | 2          | 2.9.7                |                  |                 |                      |                    |
| crash                          | 2          | 8.0.1                |                  | xuchun-shang    |                      |                    |
| cronie                         | 2          | 1.5.7                | 是               | carlo_bai       |                      |                    |
| crontabs                       | 2          | 1.11                 |                  | carlo_bai       |                      |                    |
| crypto-policies                | 2          | 20220314             |                  |                 |                      |                    |
| cryptsetup                     | 2          | 2.4.3                |                  |                 |                      |                    |
| curl                           | 2          | 7.86.0               | 是               | 扣肉酱          |                      |                    |
| dbxtool                        | 2          | 8                    |                  |                 |                      |                    |
| dhcp                           | 2          | 4.4.3                |                  |                 |                      |                    |
| dmidecode                      | 2          | 3.4                  |                  |                 |                      |                    |
| dnsmasq                        | 2          | 2.86                 |                  |                 |                      |                    |
| dracut                         | 2          | 057                  | 是               | su-feng-nmg     |                      |                    |
| e2fsprogs                      | 2          | 1.46.5               |                  | taijuanZZY      | 是                   |                    |
| efibootmgr                     | 2          | 17                   |                  | xuchun-shang    |                      |                    |
| elfutils                       | 2          | 0.188                | 是               | 扣肉酱          |                      |                    |
| ethtool                        | 2          | 5.19                 |                  | Alihq           |                      |                    |
| file                           | 2          | 5.41                 |                  |                 |                      |                    |
| findutils                      | 2          | 4.9.0                |                  | dian-ws         |                      |                    |
| firewalld                      | 2          | 1.2.1                | 是               | fr4nk2          |                      |                    |
| freetype                       | 2          | 2.12.1               |                  |                 |                      |                    |
| glib2                          | 2          | 2.73.2               | 是               | 扣肉酱          |                      |                    |
| gnutls                         | 2          | 3.7.8                |                  |                 |                      |                    |
| golang                         | 2          | 1.18.3               |                  |                 |                      |                    |
| grubby                         | 2          | 8.40                 |                  | fe_zhang        |                      |                    |
| hwdata                         | 2          | 0.356                | 是               | 扣肉酱          |                      |                    |
| initscripts                    | 2          | 10.17                |                  |                 |                      |                    |
| iproute                        | 2          | 5.9.0                |                  | Alihq           |                      |                    |
| iptables                       | 2          | 1.8.8                |                  | Alihq           |                      |                    |
| kexec-tools                    | 2          | 2.0.25               |                  | xuchun-shang    |                      |                    |
| keyutils                       | 2          | 1.6.1                |                  |                 |                      |                    |
| kmod                           | 2          | 30                   |                  |                 |                      |                    |
| libedit                        | 2          | 3.1                  |                  | zhangjing-gitee |                      |                    |
| libselinux                     | 2          | 3.4                  |                  |                 |                      |                    |
| libtool                        | 2          | 2.4.7                |                  | su-feng-nmg     |                      |                    |
| liburing                       | 2          | 2.1                  |                  | taijuanZZY      | 是                   | 待明确版本可能降级 |
| libxcrypt                      | 2          | 4.4.28               |                  |                 |                      |                    |
| libxml2                        | 2          | 2.9.14               |                  |                 |                      |                    |
| llvm                           | 2          | 13.0.1               |                  |                 |                      |                    |
| logrotate                      | 2          | 3.20.1               |                  |                 |                      |                    |
| lsof                           | 2          | 4.95.0               |                  | fr4nk2          |                      |                    |
| lvm2                           | 2          | 2.03.15              | 是               | 扣肉酱          |                      |                    |
| NetworkManager                 | 2          | 1.36.2               |                  | Alihq           |                      |                    |
| openssh                        | 2          | 8.8p1                |                  |                 |                      |                    |
| pam                            | 2          | 1.5.2                |                  | kun-llfl        |                      |                    |
| parted                         | 2          | 3.5                  |                  |                 |                      |                    |
| passwd                         | 2          | 0.80                 |                  |                 |                      |                    |
| pciutils                       | 2          | 3.8.0                |                  | dian-ws         |                      |                    |
| pcre                           | 2          | 8.45                 |                  |                 |                      |                    |
| pcre2                          | 2          | 10.40                |                  |                 |                      |                    |
| perl                           | 2          | 5.34.0               |                  |                 |                      |                    |
| policycoreutils                | 2          | 3.4                  |                  |                 |                      |                    |
| popt                           | 2          | 1.19                 |                  | zhangjing-gitee |                      |                    |
| procps-ng                      | 2          | 3.3.17               |                  |                 |                      |                    |
| python3.10                     | 2          | 3.10.6               |                  |                 |                      |                    |
| readline                       | 2          | 8.1                  |                  | zhangjing-gitee |                      |                    |
| rng-tools                      | 2          | 6.15                 |                  |                 |                      |                    |
| rpcbind                        | 2          | 1.2.6                |                  |                 |                      |                    |
| rsyslog                        | 2          | 8.2112.0             |                  |                 |                      |                    |
| rust                           | 2          | 1.59.0               |                  | xuchun-shang    |                      |                    |
| selinux-policy                 | 2          | 36.4                 |                  |                 |                      |                    |
| shadow-utils                   | 2          | 4.12.3               |                  |                 |                      |                    |
| shim                           | 2          | 15.4                 |                  | fr4nk2          |                      |                    |
| sqlite                         | 2          | 3.40.0               | 是               | 扣肉酱          |                      |                    |
| sssd                           | 2          | 2.7.4                |                  |                 |                      |                    |
| sudo                           | 2          | 1.9.10               | 是               | kun-llfl        |                      |                    |
| syslinux                       | 2          | 6.04~pre1            |                  |                 |                      |                    |
| system-storage-manager         | 2          | 1.4                  |                  |                 |                      |                    |
| tar                            | 2          | 1.34                 |                  | dian-ws         |                      |                    |
| tcl                            | 2          | 8.6.12               |                  |                 |                      |                    |
| tuned                          | 2          | 2.18.0               |                  | su-feng-nmg     |                      |                    |
| unzip                          | 2          | 6.0                  | 是               | zhangjing-gitee |                      |                    |
| which                          | 2          | 2.21                 |                  | fe_zhang        |                      |                    |
| xfsprogs                       | 2          | 5.10.0               | 是               | taijuanZZY      | 是                   |                    |
| zlib                           | 2          | 1.2.12               | 是               | 扣肉酱          |                      |                    |
| zstd                           | 2          | 1.5.2                |                  | dian-ws         |                      |                    |
| abattis-cantarell-fonts        | 3          | 0.303.1              |                  |                 |                      |                    |
| abi-compliance-checker         | 3          | 2.3                  |                  |                 |                      |                    |
| abi-dumper                     | 3          | 1.2                  |                  |                 |                      |                    |
| accel-config                   | 3          | 3.4.6.4              |                  | xuchun-shang    |                      |                    |
| accountsservice                | 3          | 22.08.8              |                  |                 |                      |                    |
| acpid                          | 3          | 2.0.33               |                  | xuchun-shang    |                      |                    |
| adcli                          | 3          | 0.9.1                |                  |                 |                      |                    |
| adobe-source-code-pro-fonts    | 3          | 2.038.1.058.1.018    |                  |                 |                      |                    |
| adwaita-icon-theme             | 3          | 42.0                 |                  |                 |                      |                    |
| alsa-lib                       | 3          | 1.2.7.2              | 是               | 扣肉酱          |                      |                    |
| annobin                        | 3          | 10.54                |                  |                 |                      |                    |
| anolis-logos                   | 3          | 23.0                 |                  |                 |                      |                    |
| apr                            | 3          | 1.7.0                |                  |                 |                      |                    |
| apr-util                       | 3          | 1.6.1                |                  |                 |                      |                    |
| argon2                         | 3          | 20190702             |                  |                 |                      |                    |
| aspell                         | 3          | 0.60.8               |                  |                 |                      |                    |
| aspell-en                      | 3          | 2020.12.07           |                  |                 |                      |                    |
| atf                            | 3          | 0.21                 |                  |                 |                      |                    |
| atk                            | 3          | 2.38.0               |                  |                 |                      |                    |
| at-spi2-atk                    | 3          | 2.38.0               |                  |                 |                      |                    |
| at-spi2-core                   | 3          | 2.44.0               |                  |                 |                      |                    |
| augeas                         | 3          | 1.13.0               |                  |                 |                      |                    |
| autoconf                       | 3          | 2.71                 |                  | fe_zhang        |                      |                    |
| autogen                        | 3          | 5.18.16              |                  | fe_zhang        |                      |                    |
| automake                       | 3          | 1.16.5               |                  | fe_zhang        |                      |                    |
| avahi                          | 3          | 0.8                  |                  |                 |                      |                    |
| babeltrace                     | 3          | 1.5.8                |                  |                 |                      |                    |
| bc                             | 3          | 1.07.1               |                  |                 |                      |                    |
| bitmap-fonts                   | 3          | 0.3                  |                  |                 |                      |                    |
| bluez                          | 3          | 5.65                 |                  |                 |                      |                    |
| boost                          | 3          | 1.78.0               | 是               | 扣肉酱          |                      |                    |
| bridge-utils                   | 3          | 1.7.1                |                  |                 |                      |                    |
| brltty                         | 3          | 6.4                  |                  |                 |                      |                    |
| brotli                         | 3          | 1.0.9                |                  |                 |                      |                    |
| btrfs-progs                    | 3          | 5.16.2               |                  | taijuanZZY      |                      |                    |
| bubblewrap                     | 3          | 0.5.0                |                  |                 |                      |                    |
| bzip2                          | 3          | 1.0.6                |                  | dian-ws         |                      |                    |
| ca-certificates                | 3          | 2021.2.52            |                  |                 |                      |                    |
| cairo                          | 3          | 1.17.6               |                  |                 |                      |                    |
| c-ares                         | 3          | 1.18.1               |                  |                 |                      |                    |
| cdparanoia                     | 3          | 10.2                 |                  |                 |                      |                    |
| ceph                           | 3          | 17.2.3               |                  | taijuanZZY      |                      |                    |
| cloud-init                     | 3          | 19.1.17              |                  |                 |                      |                    |
| cloud-utils-growpart           | 3          | 0.32                 |                  |                 |                      |                    |
| cmake                          | 3          | 3.25.0               | 是               | 扣肉酱          |                      |                    |
| colord                         | 3          | 1.4.6                |                  |                 |                      |                    |
| colord-gtk                     | 3          | 0.3.0                |                  |                 |                      |                    |
| color-filesystem               | 3          | 23                   |                  |                 |                      |                    |
| compiler-rt                    | 3          | 13.0.1               |                  |                 |                      |                    |
| conntrack-tools                | 3          | 1.4.6                |                  |                 |                      |                    |
| cpu_features                   | 3          | 0.7.0                | 是               | happy_orange    |                      |                    |
| createrepo_c                   | 3          | 0.20.0               |                  |                 |                      |                    |
| criu                           | 3          | 3.17.1               |                  |                 |                      |                    |
| ctags                          | 3          | 5.9                  |                  |                 |                      |                    |
| cups                           | 3          | 2.4.2                |                  |                 |                      |                    |
| cups-filters                   | 3          | 1.28.15              |                  |                 |                      |                    |
| cups-pk-helper                 | 3          | 0.2.6                |                  |                 |                      |                    |
| cyrus-sasl                     | 3          | 2.1.28               |                  |                 |                      |                    |
| dbus-glib                      | 3          | 0.112                |                  |                 |                      |                    |
| dbus-python                    | 3          | 1.2.18               |                  |                 |                      |                    |
| dconf                          | 3          | 0.40.0               |                  |                 |                      |                    |
| debugedit                      | 3          | 5.0                  |                  |                 |                      |                    |
| device-mapper-multipath        | 3          | 0.8.8                |                  |                 |                      |                    |
| device-mapper-persistent-data  | 3          | 0.9.0                |                  |                 |                      |                    |
| diffutils                      | 3          | 3.8                  |                  |                 |                      |                    |
| ding-libs                      | 3          | 0.6.2                |                  |                 |                      |                    |
| dml                            | 3          | 0.1.9~beta           | 是               | happy_orange    |                      |                    |
| dmraid                         | 3          | 1.0.0.rc16           |                  |                 |                      |                    |
| dnf-plugins-core               | 3          | 4.0.24               |                  |                 |                      |                    |
| docbook5-style-xsl             | 3          | 1.79.2               |                  |                 |                      |                    |
| docbook-style-xsl              | 3          | 1.79.2               |                  |                 |                      |                    |
| docbook-utils                  | 3          | 0.6.14               |                  |                 |                      |                    |
| dosfstools                     | 3          | 4.2                  |                  |                 |                      |                    |
| drpm                           | 3          | 0.5.1                |                  |                 |                      |                    |
| dtc                            | 3          | 1.6.1                |                  |                 |                      |                    |
| dump                           | 3          | 0.4                  |                  |                 |                      |                    |
| dwarves                        | 3          | 1.23                 |                  |                 |                      |                    |
| dwz                            | 3          | 0.14                 |                  |                 |                      |                    |
| dyninst                        | 3          | 12.1.0               |                  |                 |                      |                    |
| ecj                            | 3          | 4.23                 |                  |                 |                      |                    |
| ed                             | 3          | 1.18                 |                  |                 |                      |                    |
| efi-rpm-macros                 | 3          | 5                    |                  |                 |                      |                    |
| efivar                         | 3          | 38                   |                  |                 |                      |                    |
| emacs                          | 3          | 27.2                 |                  |                 |                      |                    |
| enchant2                       | 3          | 2.3.3                |                  |                 |                      |                    |
| exempi                         | 3          | 2.6.1                |                  |                 |                      |                    |
| exiv2                          | 3          | 0.27.5               |                  |                 |                      |                    |
| expat                          | 3          | 2.4.8                |                  |                 |                      |                    |
| fakeroot                       | 3          | 1.28                 |                  |                 |                      |                    |
| fcoe-utils                     | 3          | 1.0.34               |                  |                 |                      |                    |
| fdk-aac-free                   | 3          | 2.0.2                |                  |                 |                      |                    |
| fftw                           | 3          | 3.3.10               |                  |                 |                      |                    |
| flac                           | 3          | 1.3.4                |                  |                 |                      |                    |
| flatpak                        | 3          | 1.13.2               | 是               | 扣肉酱          |                      |                    |
| flex                           | 3          | 2.6.4                |                  |                 |                      |                    |
| flexiblas                      | 3          | 3.2.1                |                  |                 |                      |                    |
| fltk                           | 3          | 1.3.8                |                  |                 |                      |                    |
| fontconfig                     | 3          | 2.14.0               | 是               | 扣肉酱          |                      |                    |
| fonts-rpm-macros               | 3          | 4.0.2                |                  |                 |                      |                    |
| fribidi                        | 3          | 1.0.11               |                  |                 |                      |                    |
| fstrm                          | 3          | 0.6.1                |                  |                 |                      |                    |
| ftp                            | 3          | 0.17                 |                  |                 |                      |                    |
| fuse                           | 3          | 3.10.5               |                  | taijuanZZY      |                      |                    |
| fwupd                          | 3          | 1.7.5                |                  |                 |                      |                    |
| gawk                           | 3          | 5.1.1                |                  |                 |                      |                    |
| gc                             | 3          | 8.2.0                |                  |                 |                      |                    |
| gcab                           | 3          | 1.5                  |                  |                 |                      |                    |
| gcr                            | 3          | 3.41.0               |                  |                 |                      |                    |
| gd                             | 3          | 2.3.3                |                  |                 |                      |                    |
| gdbm                           | 3          | 1.23                 |                  |                 |                      |                    |
| gdisk                          | 3          | 1.0.8                |                  |                 |                      |                    |
| gdk-pixbuf2                    | 3          | 2.42.9               |                  |                 |                      |                    |
| geoclue2                       | 3          | 2.6.0                |                  |                 |                      |                    |
| geocode-glib                   | 3          | 3.26.2               |                  |                 |                      |                    |
| geolite2                       | 3          | 20191217             |                  |                 |                      |                    |
| gettext                        | 3          | 0.21                 |                  |                 |                      |                    |
| giflib                         | 3          | 5.2.1                |                  |                 |                      |                    |
| git                            | 3          | 2.38.1               | 是               | 扣肉酱          |                      |                    |
| gjs                            | 3          | 1.70.2               |                  |                 |                      |                    |
| glib-networking                | 3          | 2.72.0               |                  |                 |                      |                    |
| gl-manpages                    | 3          | 1.1                  |                  |                 |                      |                    |
| gmp                            | 3          | 6.2.1                |                  |                 |                      |                    |
| gnome-control-center           | 3          | 42.0                 |                  |                 |                      |                    |
| gnome-desktop3                 | 3          | 42.0                 |                  |                 |                      |                    |
| gnome-kiosk                    | 3          | 42.0                 |                  |                 |                      |                    |
| gnome-settings-daemon          | 3          | 42.1                 |                  |                 |                      |                    |
| gnupg2                         | 3          | 2.3.7                | 是               | 扣肉酱          |                      |                    |
| gobject-introspection          | 3          | 1.71.0               |                  |                 |                      |                    |
| go-compilers                   | 3          | 1                    |                  |                 |                      |                    |
| google-noto-cjk-fonts          | 3          | 20220722             |                  |                 |                      |                    |
| google-noto-fonts              | 3          | 20201206             |                  |                 |                      |                    |
| go-rpm-macros                  | 3          | 3.0.15               |                  |                 |                      |                    |
| gpgme                          | 3          | 1.18.0               |                  |                 |                      |                    |
| gpm                            | 3          | 1.20.7               |                  |                 |                      |                    |
| graphene                       | 3          | 1.10.8               |                  |                 |                      |                    |
| graphite2                      | 3          | 1.3.14               |                  |                 |                      |                    |
| grep                           | 3          | 3.7                  | 是               | 扣肉酱          |                      |                    |
| groff                          | 3          | 1.22.4               |                  |                 |                      |                    |
| gsettings-desktop-schemas      | 3          | 42.0                 |                  |                 |                      |                    |
| gsl                            | 3          | 2.7.1                | 是               | happy_orange    |                      |                    |
| gsm                            | 3          | 1.0.22               |                  |                 |                      |                    |
| gsound                         | 3          | 1.0.3                |                  |                 |                      |                    |
| gssdp                          | 3          | 1.4.0.1              |                  |                 |                      |                    |
| gssproxy                       | 3          | 0.8.4                |                  |                 |                      |                    |
| gstreamer1                     | 3          | 1.20.1               | 是               | 扣肉酱          |                      |                    |
| gstreamer1-plugins-bad-free    | 3          | 1.20.1               | 是               | 扣肉酱          |                      |                    |
| gstreamer1-plugins-base        | 3          | 1.20.1               | 是               | 扣肉酱          |                      |                    |
| gstreamer1-plugins-good        | 3          | 1.20.1               |                  |                 |                      |                    |
| gtest                          | 3          | 1.12.1               | 是               | happy_orange    |                      |                    |
| gtk2                           | 3          | 2.24.33              |                  |                 |                      |                    |
| gtk3                           | 3          | 3.24.34              | 是               | 扣肉酱          |                      |                    |
| gtk4                           | 3          | 4.7.0                |                  |                 |                      |                    |
| guile                          | 3          | 3.0.8                |                  |                 |                      |                    |
| gupnp                          | 3          | 1.4.3                |                  |                 |                      |                    |
| gupnp-igd                      | 3          | 1.2.0                |                  |                 |                      |                    |
| gzip                           | 3          | 1.12                 |                  | dian-ws         |                      |                    |
| harfbuzz                       | 3          | 4.2.0                |                  |                 |                      |                    |
| hdparm                         | 3          | 9.63                 |                  |                 |                      |                    |
| helib                          | 3          | 2.2.1                | 是               | happy_orange    |                      |                    |
| hexedit                        | 3          | 1.5                  |                  |                 |                      |                    |
| hexl                           | 3          | 1.2.3                | 是               | happy_orange    |                      |                    |
| hfsplus-tools                  | 3          | 540.1.linux3         |                  |                 |                      |                    |
| hicolor-icon-theme             | 3          | 0.17                 |                  |                 |                      |                    |
| hidapi                         | 3          | 0.11.2               |                  |                 |                      |                    |
| hostname                       | 3          | 3.23                 |                  |                 |                      |                    |
| http-parser                    | 3          | 2.9.4                |                  |                 |                      |                    |
| hunspell                       | 3          | 1.7.1                |                  |                 |                      |                    |
| hunspell-en                    | 3          | 0.2020.12.07         |                  |                 |                      |                    |
| hyphen                         | 3          | 2.8.8                |                  |                 |                      |                    |
| ibus                           | 3          | 1.5.26               | 是               | 扣肉酱          |                      |                    |
| icu                            | 3          | 71.1                 |                  |                 |                      |                    |
| iio-sensor-proxy               | 3          | 3.3                  |                  |                 |                      |                    |
| ima-evm-utils                  | 3          | 1.4                  |                  |                 |                      |                    |
| inih                           | 3          | 56                   |                  |                 |                      |                    |
| iniparser                      | 3          | 4.1                  |                  |                 |                      |                    |
| intel-ipp-crypto-mb            | 3          | 1.0.5                | 是               | happy_orange    |                      |                    |
| intel-ipsec-mb                 | 3          | 1.3.0                | 是               | happy_orange    |                      |                    |
| ipcalc                         | 3          | 1.0.1                |                  |                 |                      |                    |
| ipmitool                       | 3          | 1.8.18               |                  | xuchun-shang    |                      |                    |
| iprutils                       | 3          | 2.4.19               |                  |                 |                      |                    |
| ipset                          | 3          | 7.15                 |                  |                 |                      |                    |
| iputils                        | 3          | 20211215             |                  |                 |                      |                    |
| ipvsadm                        | 3          | 1.31                 |                  |                 |                      |                    |
| ipxe                           | 3          | 20220210.git64113751 |                  |                 |                      |                    |
| irqbalance                     | 3          | 1.8.0                |                  |                 |                      |                    |
| iscsi-initiator-utils          | 3          | 2.1.6                |                  |                 |                      |                    |
| isns-utils                     | 3          | 0.101                |                  |                 |                      |                    |
| iso-codes                      | 3          | 4.9.0                |                  |                 |                      |                    |
| isomd5sum                      | 3          | 1.2.3                |                  |                 |                      |                    |
| iw                             | 3          | 5.9                  |                  |                 |                      |                    |
| jansson                        | 3          | 2.14                 |                  |                 |                      |                    |
| java-1.8.0-alibaba-dragonwell  | 3          | 1.8.0.345            | 是               | happy_orange    | 是                   |                    |
| java-11-alibaba-dragonwell     | 3          | 11.0.16.12.8         | 是               | happy_orange    | 是                   |                    |
| java-17-alibaba-dragonwell     | 3          | 17.0.4.0.4.8         | 是               | happy_orange    | 是                   |                    |
| javapackages-tools             | 3          | 6.0.0                |                  |                 |                      |                    |
| jbigkit                        | 3          | 2.1                  |                  |                 |                      |                    |
| jitterentropy                  | 3          | 3.4.0                |                  |                 |                      |                    |
| js-jquery                      | 3          | 3.6.0                |                  |                 |                      |                    |
| json-c                         | 3          | 0.16                 |                  |                 |                      |                    |
| json-glib                      | 3          | 1.6.6                |                  |                 |                      |                    |
| kbd                            | 3          | 2.5.1                |                  |                 |                      |                    |
| keentuned                      | 3          | 1.1.3                | 是               | happy_orange    | 是                   |                    |
| keentune-target                | 3          | 1.1.3                | 是               | happy_orange    | 是                   |                    |
| keybinder3                     | 3          | 0.3.2                |                  |                 |                      |                    |
| krb5                           | 3          | 1.19.2               |                  |                 |                      |                    |
| kyua                           | 3          | 0.13                 |                  |                 |                      |                    |
| lame                           | 3          | 3.100                |                  |                 |                      |                    |
| langpacks                      | 3          | 3.0                  |                  |                 |                      |                    |
| langtable                      | 3          | 0.0.57               |                  |                 |                      |                    |
| lcms2                          | 3          | 2.13.1               |                  |                 |                      |                    |
| less                           | 3          | 590                  | 是               | 扣肉酱          |                      |                    |
| libabigail                     | 3          | 2.0                  |                  |                 |                      |                    |
| libadwaita                     | 3          | 1.1.1                |                  |                 |                      |                    |
| libaio                         | 3          | 0.3.112              |                  |                 |                      |                    |
| libappstream-glib              | 3          | 0.8.1                | 是               | 扣肉酱          |                      |                    |
| libarchive                     | 3          | 3.6.1                |                  |                 |                      |                    |
| libassuan                      | 3          | 2.5.5                |                  |                 |                      |                    |
| libasyncns                     | 3          | 0.8                  |                  |                 |                      |                    |
| libatasmart                    | 3          | 0.19                 |                  |                 |                      |                    |
| libblockdev                    | 3          | 2.28                 |                  |                 |                      |                    |
| libbpf                         | 3          | 0.7.0                |                  |                 |                      |                    |
| libbytesize                    | 3          | 2.7                  |                  |                 |                      |                    |
| libcanberra                    | 3          | 0.30                 |                  |                 |                      |                    |
| libcap                         | 3          | 2.63                 |                  |                 |                      |                    |
| libcap-ng                      | 3          | 0.8.3                |                  |                 |                      |                    |
| libcbor                        | 3          | 0.7.0                |                  |                 |                      |                    |
| libcgroup                      | 3          | 2.0                  |                  |                 |                      |                    |
| libcloudproviders              | 3          | 0.3.1                |                  |                 |                      |                    |
| libcomps                       | 3          | 0.1.18               |                  |                 |                      |                    |
| libconfig                      | 3          | 1.7.3                |                  |                 |                      |                    |
| libcxxabi                      | 3          | 13.0.1               |                  |                 |                      |                    |
| libdaemon                      | 3          | 0.14                 |                  |                 |                      |                    |
| libdatrie                      | 3          | 0.2.13               |                  |                 |                      |                    |
| libdnf                         | 3          | 0.65.0               |                  |                 |                      |                    |
| libdrm                         | 3          | 2.4.110              |                  |                 |                      |                    |
| libdvdnav                      | 3          | 6.1.1                |                  |                 |                      |                    |
| libdvdread                     | 3          | 6.1.3                |                  |                 |                      |                    |
| libeconf                       | 3          | 0.4.0                |                  |                 |                      |                    |
| libepoxy                       | 3          | 1.5.10               |                  |                 |                      |                    |
| libestr                        | 3          | 0.1.11               |                  |                 |                      |                    |
| libev                          | 3          | 4.33                 |                  |                 |                      |                    |
| libevdev                       | 3          | 1.12.1               | 是               | 扣肉酱          |                      |                    |
| libevent                       | 3          | 2.1.12               |                  |                 |                      |                    |
| libexif                        | 3          | 0.6.24               |                  |                 |                      |                    |
| libfastjson                    | 3          | 0.99.9               |                  |                 |                      |                    |
| libffi                         | 3          | 3.4.2                |                  |                 |                      |                    |
| libfido2                       | 3          | 1.11.0               |                  |                 |                      |                    |
| libfontenc                     | 3          | 1.1.4                | 是               | 扣肉酱          |                      |                    |
| libgcrypt                      | 3          | 1.10.1               |                  |                 |                      |                    |
| libgexiv2                      | 3          | 0.14.0               |                  |                 |                      |                    |
| libgit2                        | 3          | 1.3.1                |                  |                 |                      |                    |
| libglvnd                       | 3          | 1.5.0                |                  |                 |                      |                    |
| libgnomekbd                    | 3          | 3.26.1               |                  |                 |                      |                    |
| libgpg-error                   | 3          | 1.46                 |                  |                 |                      |                    |
| libgsf                         | 3          | 1.14.49              |                  |                 |                      |                    |
| libgtop2                       | 3          | 2.40.0               |                  |                 |                      |                    |
| libgudev                       | 3          | 237                  |                  |                 |                      |                    |
| libgusb                        | 3          | 0.4.0                |                  |                 |                      |                    |
| libgweather                    | 3          | 4.0.0                |                  |                 |                      |                    |
| libgxps                        | 3          | 0.3.2                |                  |                 |                      |                    |
| libhandy                       | 3          | 1.6.1                |                  |                 |                      |                    |
| libical                        | 3          | 3.0.14               |                  |                 |                      |                    |
| libICE                         | 3          | 1.0.10               |                  |                 |                      |                    |
| libidn2                        | 3          | 2.3.3                | 是               | 扣肉酱          |                      |                    |
| libimobiledevice               | 3          | 1.3.0                |                  |                 |                      |                    |
| libinput                       | 3          | 1.20.0               |                  |                 |                      |                    |
| libipt                         | 3          | 2.0.5                |                  |                 |                      |                    |
| libiptcdata                    | 3          | 1.0.5                |                  |                 |                      |                    |
| libjpeg-turbo                  | 3          | 2.1.4                |                  |                 |                      |                    |
| libkcapi                       | 3          | 1.4.0                |                  |                 |                      |                    |
| libksba                        | 3          | 1.6.2                |                  |                 |                      |                    |
| libldac                        | 3          | 2.0.2.3              |                  |                 |                      |                    |
| libldb                         | 3          | 2.5.0                | 是               | 扣肉酱          |                      |                    |
| liblockfile                    | 3          | 1.17                 |                  |                 |                      |                    |
| libmaxminddb                   | 3          | 1.6.0                |                  |                 |                      |                    |
| libmbim                        | 3          | 1.26.2               |                  |                 |                      |                    |
| libmetalink                    | 3          | 0.1.3                |                  |                 |                      |                    |
| libmnl                         | 3          | 1.0.4                |                  |                 |                      |                    |
| libmodulemd                    | 3          | 2.14.0               |                  |                 |                      |                    |
| libmpc                         | 3          | 1.2.1                |                  |                 |                      |                    |
| libndp                         | 3          | 1.8                  |                  |                 |                      |                    |
| libnet                         | 3          | 1.2                  |                  |                 |                      |                    |
| libnetfilter_conntrack         | 3          | 1.0.9                |                  |                 |                      |                    |
| libnetfilter_cthelper          | 3          | 1.0.0                |                  |                 |                      |                    |
| libnetfilter_cttimeout         | 3          | 1.0.0                |                  |                 |                      |                    |
| libnetfilter_queue             | 3          | 1.0.5                |                  |                 |                      |                    |
| libnfnetlink                   | 3          | 1.0.1                |                  |                 |                      |                    |
| libnftnl                       | 3          | 1.2.3                |                  |                 |                      |                    |
| libnice                        | 3          | 0.1.18               |                  |                 |                      |                    |
| libnl3                         | 3          | 3.7.0                |                  |                 |                      |                    |
| libnma                         | 3          | 1.8.36               |                  |                 |                      |                    |
| libnotify                      | 3          | 0.8.1                |                  |                 |                      |                    |
| libnsl2                        | 3          | 2.0.0                |                  |                 |                      |                    |
| libogg                         | 3          | 1.3.5                |                  |                 |                      |                    |
| libosinfo                      | 3          | 1.10.0               |                  |                 |                      |                    |
| libpcap                        | 3          | 1.10.1               |                  |                 |                      |                    |
| libpciaccess                   | 3          | 0.16                 | 是               | 扣肉酱          |                      |                    |
| libpipeline                    | 3          | 1.5.5                |                  |                 |                      |                    |
| libplist                       | 3          | 2.2.0                |                  |                 |                      |                    |
| libpng                         | 3          | 1.6.38               |                  |                 |                      |                    |
| libpq                          | 3          | 14.1                 |                  |                 |                      |                    |
| libproxy                       | 3          | 0.4.18               |                  |                 |                      |                    |
| libpsl                         | 3          | 0.21.1               |                  |                 |                      |                    |
| libpwquality                   | 3          | 1.4.4                |                  |                 |                      |                    |
| libqmi                         | 3          | 1.30.4               | 是               | 扣肉酱          |                      |                    |
| libqrtr-glib                   | 3          | 1.2.2                |                  |                 |                      |                    |
| librelp                        | 3          | 1.10.0               |                  |                 |                      |                    |
| librepo                        | 3          | 1.14.2               |                  |                 |                      |                    |
| libreport                      | 3          | 2.17.1               |                  |                 |                      |                    |
| librsvg2                       | 3          | 2.54.0               |                  |                 |                      |                    |
| libseccomp                     | 3          | 2.5.3                |                  |                 |                      |                    |
| libsecret                      | 3          | 0.20.4               |                  |                 |                      |                    |
| libsemanage                    | 3          | 3.4                  |                  |                 |                      |                    |
| libsepol                       | 3          | 3.4                  |                  |                 |                      |                    |
| libserf                        | 3          | 1.3.9                |                  |                 |                      |                    |
| libshout                       | 3          | 2.4.5                |                  |                 |                      |                    |
| libsigsegv                     | 3          | 2.14                 |                  |                 |                      |                    |
| libSM                          | 3          | 1.2.3                |                  |                 |                      |                    |
| libsmbios                      | 3          | 2.4.3                |                  |                 |                      |                    |
| libsndfile                     | 3          | 1.1.0                |                  |                 |                      |                    |
| libsolv                        | 3          | 0.7.20               |                  |                 |                      |                    |
| libsoup                        | 3          | 2.74.2               |                  |                 |                      |                    |
| libsrtp                        | 3          | 2.4.2                |                  |                 |                      |                    |
| libssh                         | 3          | 0.10.4               |                  |                 |                      |                    |
| libssh2                        | 3          | 1.10.0               |                  |                 |                      |                    |
| libstemmer                     | 3          | 2.2.0                |                  |                 |                      |                    |
| libtalloc                      | 3          | 2.3.4                |                  |                 |                      |                    |
| libtasn1                       | 3          | 4.19.0               |                  |                 |                      |                    |
| libtdb                         | 3          | 1.4.6                | 是               | 扣肉酱          |                      |                    |
| libteam                        | 3          | 1.31                 |                  |                 |                      |                    |
| libtevent                      | 3          | 0.13.0               |                  |                 |                      |                    |
| libthai                        | 3          | 0.1.29               |                  |                 |                      |                    |
| libtheora                      | 3          | 1.1.1                |                  |                 |                      |                    |
| libtiff                        | 3          | 4.4.0                |                  |                 |                      |                    |
| libtimezonemap                 | 3          | 0.4.5.2              |                  |                 |                      |                    |
| libtirpc                       | 3          | 1.3.3                |                  |                 |                      |                    |
| libtraceevent                  | 3          | 1.5.2                |                  |                 |                      |                    |
| libunistring                   | 3          | 1.0                  | 是               | 扣肉酱          |                      |                    |
| libunwind                      | 3          | 1.6.2                |                  |                 |                      |                    |
| libusbmuxd                     | 3          | 2.0.2                |                  |                 |                      |                    |
| libusbx                        | 3          | 1.0.25               |                  |                 |                      |                    |
| libuser                        | 3          | 0.63                 |                  |                 |                      |                    |
| libutempter                    | 3          | 1.2.1                |                  |                 |                      |                    |
| libuv                          | 3          | 1.44.0               |                  |                 |                      |                    |
| libva                          | 3          | 2.14.0               |                  |                 |                      |                    |
| libverto                       | 3          | 0.3.2                |                  |                 |                      |                    |
| libvisual                      | 3          | 0.4.0                |                  |                 |                      |                    |
| libvorbis                      | 3          | 1.3.7                |                  |                 |                      |                    |
| libvpx                         | 3          | 1.11.0               |                  |                 |                      |                    |
| libwacom                       | 3          | 2.2.0                |                  |                 |                      |                    |
| libwebp                        | 3          | 1.2.4                |                  |                 |                      |                    |
| libwpe                         | 3          | 1.14.0               |                  |                 |                      |                    |
| libX11                         | 3          | 1.7.4                |                  |                 |                      |                    |
| libXau                         | 3          | 1.0.9                |                  |                 |                      |                    |
| libXaw                         | 3          | 1.0.14               |                  |                 |                      |                    |
| libxcb                         | 3          | 1.14                 |                  |                 |                      |                    |
| libXcomposite                  | 3          | 0.4.5                |                  |                 |                      |                    |
| libXcursor                     | 3          | 1.2.1                |                  |                 |                      |                    |
| libXdamage                     | 3          | 1.1.5                |                  |                 |                      |                    |
| libXdmcp                       | 3          | 1.1.3                | 是               | 扣肉酱          |                      |                    |
| libXext                        | 3          | 1.3.5                | 是               | 扣肉酱          |                      |                    |
| libXfixes                      | 3          | 6.0.0                |                  |                 |                      |                    |
| libXfont2                      | 3          | 2.0.5                | 是               | 扣肉酱          |                      |                    |
| libXft                         | 3          | 2.3.7                | 是               | 扣肉酱          |                      |                    |
| libXi                          | 3          | 1.8                  |                  |                 |                      |                    |
| libXinerama                    | 3          | 1.1.5                | 是               | 扣肉酱          |                      |                    |
| libxkbcommon                   | 3          | 1.4.0                |                  |                 |                      |                    |
| libxkbfile                     | 3          | 1.1.0                | 是               | 扣肉酱          |                      |                    |
| libxklavier                    | 3          | 5.4                  |                  |                 |                      |                    |
| libxmlb                        | 3          | 0.3.6                |                  |                 |                      |                    |
| libXmu                         | 3          | 1.1.3                |                  |                 |                      |                    |
| libXpm                         | 3          | 3.5.13               | 是               | 扣肉酱          |                      |                    |
| libXrandr                      | 3          | 1.5.2                | 是               | 扣肉酱          |                      |                    |
| libXrender                     | 3          | 0.9.11               | 是               | 扣肉酱          |                      |                    |
| libXScrnSaver                  | 3          | 1.2.3                |                  |                 |                      |                    |
| libxshmfence                   | 3          | 1.3                  | 是               | 扣肉酱          |                      |                    |
| libxslt                        | 3          | 1.1.35               |                  |                 |                      |                    |
| libXt                          | 3          | 1.2.1                |                  |                 |                      |                    |
| libXtst                        | 3          | 1.2.3                | 是               | 扣肉酱          |                      |                    |
| libXv                          | 3          | 1.0.11               |                  |                 |                      |                    |
| libXvMC                        | 3          | 1.0.13               |                  |                 |                      |                    |
| libXxf86vm                     | 3          | 1.1.4                | 是               | 扣肉酱          |                      |                    |
| libyaml                        | 3          | 0.2.5                |                  |                 |                      |                    |
| lksctp-tools                   | 3          | 1.0.18               |                  |                 |                      |                    |
| lld                            | 3          | 13.0.1               |                  |                 |                      |                    |
| lldpad                         | 3          | 1.1                  |                  |                 |                      |                    |
| lm_sensors                     | 3          | 3.6.0                |                  |                 |                      |                    |
| lmdb                           | 3          | 0.9.29               |                  |                 |                      |                    |
| log4cpp                        | 3          | 1.1.3                |                  |                 |                      |                    |
| lohit-assamese-fonts           | 3          | 2.91.5               |                  |                 |                      |                    |
| lohit-bengali-fonts            | 3          | 2.91.5               |                  |                 |                      |                    |
| lohit-devanagari-fonts         | 3          | 2.95.4               |                  |                 |                      |                    |
| lohit-gujarati-fonts           | 3          | 2.92.4               |                  |                 |                      |                    |
| lohit-kannada-fonts            | 3          | 2.5.4                |                  |                 |                      |                    |
| lohit-marathi-fonts            | 3          | 2.94.2               |                  |                 |                      |                    |
| lohit-odia-fonts               | 3          | 2.91.2               |                  |                 |                      |                    |
| lohit-tamil-fonts              | 3          | 2.91.3               |                  |                 |                      |                    |
| lohit-telugu-fonts             | 3          | 2.5.5                |                  |                 |                      |                    |
| lorax                          | 3          | 37.0                 |                  |                 |                      |                    |
| lorax-templates-anolis         | 3          | 37.0                 |                  |                 |                      |                    |
| lshw                           | 3          | B.02.19.2            |                  |                 |                      |                    |
| lsscsi                         | 3          | 0.32                 |                  |                 |                      |                    |
| lua                            | 3          | 5.4.4                |                  |                 |                      |                    |
| lutok                          | 3          | 0.4                  |                  |                 |                      |                    |
| lz4                            | 3          | 1.9.3                |                  |                 |                      |                    |
| lzo                            | 3          | 2.10                 |                  |                 |                      |                    |
| m4                             | 3          | 1.4.19               |                  |                 |                      |                    |
| mailcap                        | 3          | 2.1.53               |                  |                 |                      |                    |
| make                           | 3          | 4.3                  | 是               |                 |                      |                    |
| malcontent                     | 3          | 0.10.4               |                  |                 |                      |                    |
| man-db                         | 3          | 2.10.1               | 是               | 扣肉酱          |                      |                    |
| mcstrans                       | 3          | 3.3                  |                  |                 |                      |                    |
| mdadm                          | 3          | 4.2                  |                  |                 |                      |                    |
| mecab                          | 3          | 0.996                |                  |                 |                      |                    |
| memstrack                      | 3          | 0.2.2                |                  |                 |                      |                    |
| mesa                           | 3          | 22.1.7               | 是               | 扣肉酱          |                      |                    |
| mesa-demos                     | 3          | 8.4.0                |                  |                 |                      |                    |
| microcode_ctl                  | 3          | 20220207             |                  |                 |                      |                    |
| microsoft-gsl                  | 3          | 3.1.0                | 是               | happy_orange    |                      |                    |
| mkfontscale                    | 3          | 1.2.2                |                  |                 |                      |                    |
| mobile-broadband-provider-info | 3          | 20220315             |                  |                 |                      |                    |
| mod_http2                      | 3          | 1.15.28              |                  |                 |                      |                    |
| ModemManager                   | 3          | 1.18.6               |                  |                 |                      |                    |
| mokutil                        | 3          | 0.5.0                |                  |                 |                      |                    |
| mozjs78                        | 3          | 78.15.0              |                  |                 |                      |                    |
| mpdecimal                      | 3          | 2.5.1                |                  |                 |                      |                    |
| mpfr                           | 3          | 4.1.0                | 是               | 扣肉酱          |                      |                    |
| mpg123                         | 3          | 1.29.3               |                  |                 |                      |                    |
| mtdev                          | 3          | 1.1.6                |                  |                 |                      |                    |
| mtools                         | 3          | 4.0.38               |                  |                 |                      |                    |
| mtr                            | 3          | 0.95                 |                  |                 |                      |                    |
| mt-st                          | 3          | 1.4                  |                  |                 |                      |                    |
| mutter                         | 3          | 42.0                 |                  |                 |                      |                    |
| nano                           | 3          | 7.0                  | 是               | 扣肉酱          |                      |                    |
| ncurses                        | 3          | 6.3                  |                  |                 |                      |                    |
| ndctl                          | 3          | 73                   |                  |                 |                      |                    |
| net-snmp                       | 3          | 5.9.3                |                  |                 |                      |                    |
| nettle                         | 3          | 3.8                  |                  |                 |                      |                    |
| net-tools                      | 3          | 2.10                 |                  |                 |                      |                    |
| network-manager-applet         | 3          | 1.26.0               |                  |                 |                      |                    |
| newt                           | 3          | 0.52.21              |                  |                 |                      |                    |
| nfs-utils                      | 3          | 2.6.2                |                  | taijuanZZY      | 是                   |                    |
| nftables                       | 3          | 1.0.1                |                  |                 |                      |                    |
| nghttp2                        | 3          | 1.49.0               |                  |                 |                      |                    |
| ninja-build                    | 3          | 1.11.0               | 是               | taijuanZZY      |                      |                    |
| nmap                           | 3          | 7.93                 |                  |                 |                      |                    |
| npth                           | 3          | 1.6                  |                  |                 |                      |                    |
| nspr                           | 3          | 4.33                 |                  |                 |                      |                    |
| nss                            | 3          | 3.75.0               |                  |                 |                      |                    |
| nss-altfiles                   | 3          | 2.23.0               |                  |                 |                      |                    |
| ntfs-3g                        | 3          | 2022.5.17            |                  |                 |                      |                    |
| ntfs-3g-system-compression     | 3          | 1.0                  |                  |                 |                      |                    |
| ntl                            | 3          | 11.5.1               | 是               | happy_orange    |                      |                    |
| numactl                        | 3          | 2.0.14               |                  |                 |                      |                    |
| numpy                          | 3          | 1.22.0               |                  |                 |                      |                    |
| oniguruma                      | 3          | 6.9.8                |                  |                 |                      |                    |
| openjpeg2                      | 3          | 2.5.0                |                  |                 |                      |                    |
| openldap                       | 3          | 2.6.3                |                  |                 |                      |                    |
| openssl1.1                     | 3          | 1.1.1q               |                  |                 |                      |                    |
| openssl-pkcs11                 | 3          | 0.4.11               |                  |                 |                      |                    |
| opus                           | 3          | 1.3.1                |                  |                 |                      |                    |
| orc                            | 3          | 0.4.32               | 是               | 扣肉酱          |                      |                    |
| osinfo-db                      | 3          | 20220214             |                  |                 |                      |                    |
| osinfo-db-tools                | 3          | 1.10.0               |                  |                 |                      |                    |
| os-prober                      | 3          | 1.79                 |                  |                 |                      |                    |
| ostree                         | 3          | 2022.1               | 是               | 扣肉酱          |                      |                    |
| p11-kit                        | 3          | 0.24.1               |                  |                 |                      |                    |
| paktype-naskh-basic-fonts      | 3          | 6.0                  |                  |                 |                      |                    |
| palisade                       | 3          | 1.11.6               | 是               | happy_orange    |                      |                    |
| pango                          | 3          | 1.50.6               |                  |                 |                      |                    |
| patch                          | 3          | 2.7.6                |                  |                 |                      |                    |
| pbzip2                         | 3          | 1.1.13               |                  | dian-ws         |                      |                    |
| pcsc-lite                      | 3          | 1.9.9                |                  |                 |                      |                    |
| pcsc-lite-ccid                 | 3          | 1.5.0                |                  |                 |                      |                    |
| perl-Algorithm-Diff            | 3          | 1.201                |                  |                 |                      |                    |
| perl-Archive-Tar               | 3          | 2.40                 |                  |                 |                      |                    |
| perl-Archive-Zip               | 3          | 1.68                 |                  |                 |                      |                    |
| perl-autodie                   | 3          | 2.34                 |                  |                 |                      |                    |
| perl-bignum                    | 3          | 0.65                 |                  |                 |                      |                    |
| perl-Carp                      | 3          | 1.50                 |                  |                 |                      |                    |
| perl-Compress-Bzip2            | 3          | 2.28                 |                  |                 |                      |                    |
| perl-Compress-Raw-Bzip2        | 3          | 2.103                |                  |                 |                      |                    |
| perl-Compress-Raw-Lzma         | 3          | 2.103                |                  |                 |                      |                    |
| perl-Compress-Raw-Zlib         | 3          | 2.103                |                  |                 |                      |                    |
| perl-Config-Perl-V             | 3          | 0.33                 |                  |                 |                      |                    |
| perl-constant                  | 3          | 1.33                 |                  |                 |                      |                    |
| perl-CPAN                      | 3          | 2.34                 |                  |                 |                      |                    |
| perl-CPAN-DistnameInfo         | 3          | 0.12                 |                  |                 |                      |                    |
| perl-CPAN-Meta                 | 3          | 2.150010             |                  |                 |                      |                    |
| perl-CPAN-Meta-Requirements    | 3          | 2.140                |                  |                 |                      |                    |
| perl-CPAN-Meta-YAML            | 3          | 0.018                |                  |                 |                      |                    |
| perl-Data-Dumper               | 3          | 2.183                |                  |                 |                      |                    |
| perl-Data-OptList              | 3          | 0.112                |                  |                 |                      |                    |
| perl-Data-Peek                 | 3          | 0.51                 |                  |                 |                      |                    |
| perl-Data-Section              | 3          | 0.200007             |                  |                 |                      |                    |
| perl-DBD-MariaDB               | 3          | 1.22                 |                  |                 |                      |                    |
| perl-DBD-MySQL                 | 3          | 4.050                |                  |                 |                      |                    |
| perl-DBI                       | 3          | 1.643                |                  |                 |                      |                    |
| perl-Devel-PPPort              | 3          | 3.68                 |                  |                 |                      |                    |
| perl-Devel-Size                | 3          | 0.83                 |                  |                 |                      |                    |
| perl-Digest                    | 3          | 1.20                 |                  |                 |                      |                    |
| perl-Digest-MD5                | 3          | 2.58                 |                  |                 |                      |                    |
| perl-Digest-SHA                | 3          | 6.02                 |                  |                 |                      |                    |
| perl-Digest-SHA1               | 3          | 2.13                 |                  |                 |                      |                    |
| perl-Encode                    | 3          | 3.16                 |                  |                 |                      |                    |
| perl-Encode-Locale             | 3          | 1.05                 |                  |                 |                      |                    |
| perl-Env                       | 3          | 1.04                 |                  |                 |                      |                    |
| perl-Error                     | 3          | 0.17029              |                  |                 |                      |                    |
| perl-experimental              | 3          | 0.027                |                  |                 |                      |                    |
| perl-Exporter                  | 3          | 5.74                 |                  |                 |                      |                    |
| perl-ExtUtils-CBuilder         | 3          | 0.280236             |                  |                 |                      |                    |
| perl-ExtUtils-Install          | 3          | 2.20                 |                  |                 |                      |                    |
| perl-ExtUtils-MakeMaker        | 3          | 7.64                 |                  |                 |                      |                    |
| perl-ExtUtils-Manifest         | 3          | 1.73                 |                  |                 |                      |                    |
| perl-ExtUtils-ParseXS          | 3          | 3.44                 |                  |                 |                      |                    |
| perl-File-Fetch                | 3          | 1.04                 |                  |                 |                      |                    |
| perl-File-HomeDir              | 3          | 1.006                |                  |                 |                      |                    |
| perl-File-Path                 | 3          | 2.18                 |                  |                 |                      |                    |
| perl-File-Temp                 | 3          | 0.2311               |                  |                 |                      |                    |
| perl-File-Which                | 3          | 1.27                 |                  |                 |                      |                    |
| perl-Filter                    | 3          | 1.60                 |                  |                 |                      |                    |
| perl-Filter-Simple             | 3          | 0.94                 |                  |                 |                      |                    |
| perl-Getopt-Long               | 3          | 2.52                 |                  |                 |                      |                    |
| perl-HTTP-Tiny                 | 3          | 0.080                |                  |                 |                      |                    |
| perl-Importer                  | 3          | 0.026                |                  |                 |                      |                    |
| perl-inc-latest                | 3          | 0.500                |                  |                 |                      |                    |
| perl-IO-Compress               | 3          | 2.103                |                  |                 |                      |                    |
| perl-IO-Compress-Lzma          | 3          | 2.103                |                  |                 |                      |                    |
| perl-IO-Socket-IP              | 3          | 0.41                 |                  |                 |                      |                    |
| perl-IO-Socket-SSL             | 3          | 2.074                |                  |                 |                      |                    |
| perl-IO-Zlib                   | 3          | 1.11                 |                  |                 |                      |                    |
| perl-IPC-Cmd                   | 3          | 1.04                 |                  |                 |                      |                    |
| perl-IPC-System-Simple         | 3          | 1.30                 |                  |                 |                      |                    |
| perl-IPC-SysV                  | 3          | 2.09                 |                  |                 |                      |                    |
| perl-JSON-PP                   | 3          | 4.07                 |                  |                 |                      |                    |
| perl-libnet                    | 3          | 3.13                 |                  |                 |                      |                    |
| perl-Locale-Maketext           | 3          | 1.30                 |                  |                 |                      |                    |
| perl-local-lib                 | 3          | 2.000028             |                  |                 |                      |                    |
| perl-Math-BigInt               | 3          | 1.9998.30            |                  |                 |                      |                    |
| perl-Math-BigInt-FastCalc      | 3          | 0.501.200            |                  |                 |                      |                    |
| perl-Math-BigRat               | 3          | 0.2622               |                  |                 |                      |                    |
| perl-MIME-Base64               | 3          | 3.16                 |                  |                 |                      |                    |
| perl-MIME-Charset              | 3          | 1.012.2              |                  |                 |                      |                    |
| perl-Module-Build              | 3          | 0.42.31              |                  |                 |                      |                    |
| perl-Module-CoreList           | 3          | 5.20220320           |                  |                 |                      |                    |
| perl-Module-Load               | 3          | 0.36                 |                  |                 |                      |                    |
| perl-Module-Load-Conditional   | 3          | 0.74                 |                  |                 |                      |                    |
| perl-Module-Metadata           | 3          | 1.000037             |                  |                 |                      |                    |
| perl-Module-Signature          | 3          | 0.88                 |                  |                 |                      |                    |
| perl-Mozilla-CA                | 3          | 20211001             |                  |                 |                      |                    |
| perl-MRO-Compat                | 3          | 0.15                 |                  |                 |                      |                    |
| perl-Net-Ping                  | 3          | 2.74                 |                  |                 |                      |                    |
| perl-Net-SSLeay                | 3          | 1.92                 |                  |                 |                      |                    |
| perl-Object-HashBase           | 3          | 0.009                |                  |                 |                      |                    |
| perl-Package-Generator         | 3          | 1.106                |                  |                 |                      |                    |
| perl-Params-Check              | 3          | 0.38                 |                  |                 |                      |                    |
| perl-Params-Util               | 3          | 1.102                |                  |                 |                      |                    |
| perl-parent                    | 3          | 0.238                |                  |                 |                      |                    |
| perl-PathTools                 | 3          | 3.75                 |                  |                 |                      |                    |
| perl-perlfaq                   | 3          | 5.20210520           |                  |                 |                      |                    |
| perl-PerlIO-via-QuotedPrint    | 3          | 0.09                 |                  |                 |                      |                    |
| perl-Perl-OSType               | 3          | 1.010                |                  |                 |                      |                    |
| perl-Pod-Checker               | 3          | 1.74                 |                  |                 |                      |                    |
| perl-Pod-Escapes               | 3          | 1.07                 |                  |                 |                      |                    |
| perl-podlators                 | 3          | 4.14                 |                  |                 |                      |                    |
| perl-Pod-Perldoc               | 3          | 3.28.01              |                  |                 |                      |                    |
| perl-Pod-Simple                | 3          | 3.43                 |                  |                 |                      |                    |
| perl-Pod-Usage                 | 3          | 2.01                 |                  |                 |                      |                    |
| perl-PPIx-Regexp               | 3          | 0.085                |                  |                 |                      |                    |
| perl-Scalar-List-Utils         | 3          | 1.60                 |                  |                 |                      |                    |
| perl-Socket                    | 3          | 2.032                |                  |                 |                      |                    |
| perl-Software-License          | 3          | 0.104001             |                  |                 |                      |                    |
| perl-srpm-macros               | 3          | 23                   |                  |                 |                      |                    |
| perl-Storable                  | 3          | 3.25                 |                  |                 |                      |                    |
| perl-Sub-Exporter              | 3          | 0.988                |                  |                 |                      |                    |
| perl-Sub-Install               | 3          | 0.928                |                  |                 |                      |                    |
| perl-Sys-Syslog                | 3          | 0.36                 |                  |                 |                      |                    |
| perl-Term-ANSIColor            | 3          | 5.01                 |                  |                 |                      |                    |
| perl-Term-Cap                  | 3          | 1.17                 |                  |                 |                      |                    |
| perl-TermReadKey               | 3          | 2.38                 |                  |                 |                      |                    |
| perl-Term-Size-Any             | 3          | 0.002                |                  |                 |                      |                    |
| perl-Term-Size-Perl            | 3          | 0.031                |                  |                 |                      |                    |
| perl-Term-Table                | 3          | 0.016                |                  |                 |                      |                    |
| perl-Test-Harness              | 3          | 3.42                 |                  |                 |                      |                    |
| perl-Test-Simple               | 3          | 1.302190             |                  |                 |                      |                    |
| perl-Text-Balanced             | 3          | 2.04                 |                  |                 |                      |                    |
| perl-Text-Diff                 | 3          | 1.45                 |                  |                 |                      |                    |
| perl-Text-Glob                 | 3          | 0.11                 |                  |                 |                      |                    |
| perl-Text-ParseWords           | 3          | 3.30                 |                  |                 |                      |                    |
| perl-Text-Tabs+Wrap            | 3          | 2021.0814            |                  |                 |                      |                    |
| perl-Text-Template             | 3          | 1.60                 |                  |                 |                      |                    |
| perl-Thread-Queue              | 3          | 3.13                 |                  |                 |                      |                    |
| perl-threads                   | 3          | 2.21                 |                  |                 |                      |                    |
| perl-threads-shared            | 3          | 1.59                 |                  |                 |                      |                    |
| perl-Tie-RefHash               | 3          | 1.40                 |                  |                 |                      |                    |
| perl-Time-HiRes                | 3          | 1.9764               |                  |                 |                      |                    |
| perl-Time-Local                | 3          | 1.30                 |                  |                 |                      |                    |
| perl-Unicode-Collate           | 3          | 1.31                 |                  |                 |                      |                    |
| perl-Unicode-LineBreak         | 3          | 2019.001             |                  |                 |                      |                    |
| perl-Unicode-Normalize         | 3          | 1.26                 |                  |                 |                      |                    |
| perl-URI                       | 3          | 5.10                 |                  |                 |                      |                    |
| perl-version                   | 3          | 0.99.29              |                  |                 |                      |                    |
| perl-YAML                      | 3          | 1.30                 |                  |                 |                      |                    |
| pigz                           | 3          | 2.7                  |                  |                 |                      |                    |
| pinentry                       | 3          | 1.2.0                |                  |                 |                      |                    |
| pipewire                       | 3          | 0.3.49               |                  |                 |                      |                    |
| pixman                         | 3          | 0.42.2               | 是               | 扣肉酱          |                      |                    |
| pkgconf                        | 3          | 1.8.0                |                  |                 |                      |                    |
| plymouth                       | 3          | 22.02.122            |                  |                 |                      |                    |
| polkit                         | 3          | 0.120                |                  |                 |                      |                    |
| polkit-gnome                   | 3          | 0.106                |                  |                 |                      |                    |
| polkit-pkla-compat             | 3          | 0.1                  |                  |                 |                      |                    |
| poppler                        | 3          | 22.08.0              | 是               | 扣肉酱          |                      |                    |
| poppler-data                   | 3          | 0.4.11               |                  |                 |                      |                    |
| ppp                            | 3          | 2.4.9                |                  |                 |                      |                    |
| prefixdevname                  | 3          | 0.1.0                |                  |                 |                      |                    |
| protobuf                       | 3          | 3.19.4               |                  |                 |                      |                    |
| protobuf-c                     | 3          | 1.4.1                |                  |                 |                      |                    |
| psmisc                         | 3          | 23.4                 |                  |                 |                      |                    |
| publicsuffix-list              | 3          | 20220309             |                  |                 |                      |                    |
| pulseaudio                     | 3          | 15.99.1              |                  |                 |                      |                    |
| pyatspi                        | 3          | 2.38.2               |                  |                 |                      |                    |
| pycairo                        | 3          | 1.21.0               |                  |                 |                      |                    |
| pygobject3                     | 3          | 3.42.2               |                  |                 |                      |                    |
| pykickstart                    | 3          | 3.37                 |                  |                 |                      |                    |
| pyparsing                      | 3          | 3.0.7                |                  |                 |                      |                    |
| pyparted                       | 3          | 3.12.0               |                  |                 |                      |                    |
| pyproject-rpm-macros           | 3          | 1.0.0                |                  |                 |                      |                    |
| pytest                         | 3          | 7.1.3                |                  |                 |                      |                    |
| python-argcomplete             | 3          | 2.0.0                |                  |                 |                      |                    |
| python-attrs                   | 3          | 21.4.0               |                  |                 |                      |                    |
| python-blivet                  | 3          | 3.4.3                |                  |                 |                      |                    |
| python-breathe                 | 3          | 4.34.0               |                  |                 |                      |                    |
| python-cffi                    | 3          | 1.15.1               |                  |                 |                      |                    |
| python-charset-normalizer      | 3          | 2.0.12               |                  |                 |                      |                    |
| python-configobj               | 3          | 5.0.6                |                  |                 |                      |                    |
| python-cryptography            | 3          | 38.0.1               |                  |                 |                      |                    |
| python-dasbus                  | 3          | 1.6                  |                  |                 |                      |                    |
| python-dateutil                | 3          | 2.8.2                |                  |                 |                      |                    |
| python-decorator               | 3          | 5.1.1                |                  |                 |                      |                    |
| python-distlib                 | 3          | 0.3.6                |                  |                 |                      |                    |
| python-distro                  | 3          | 1.7.0                |                  |                 |                      |                    |
| python-dns                     | 3          | 2.2.1                |                  |                 |                      |                    |
| python-editables               | 3          | 0.3                  |                  |                 |                      |                    |
| python-filelock                | 3          | 3.8.0                |                  |                 |                      |                    |
| python-hatchling               | 3          | 1.11.0               |                  |                 |                      |                    |
| python-hatch-vcs               | 3          | 0.2.0                |                  |                 |                      |                    |
| python-hidapi                  | 3          | 0.11.2               |                  |                 |                      |                    |
| python-idna                    | 3          | 3.4                  |                  |                 |                      |                    |
| python-jinja2                  | 3          | 3.1.1                |                  |                 |                      |                    |
| python-jsonpatch               | 3          | 1.32                 |                  |                 |                      |                    |
| python-jsonpointer             | 3          | 2.2                  |                  |                 |                      |                    |
| python-jsonschema              | 3          | 4.4.0                |                  |                 |                      |                    |
| python-jwt                     | 3          | 2.3.0                |                  |                 |                      |                    |
| python-linux-procfs            | 3          | 0.7.0                |                  |                 |                      |                    |
| python-lxml                    | 3          | 4.8.0                |                  |                 |                      |                    |
| python-mako                    | 3          | 1.1.4                |                  |                 |                      |                    |
| python-markupsafe              | 3          | 2.1.1                |                  |                 |                      |                    |
| python-meh                     | 3          | 0.50.1               |                  |                 |                      |                    |
| python-netaddr                 | 3          | 0.8.0                |                  |                 |                      |                    |
| python-netifaces               | 3          | 0.11.0               |                  |                 |                      |                    |
| python-networkx                | 3          | 2.8.3                |                  |                 |                      |                    |
| python-oauthlib                | 3          | 3.2.0                |                  |                 |                      |                    |
| python-packaging               | 3          | 21.3                 |                  |                 |                      |                    |
| python-pathspec                | 3          | 0.10.1               |                  |                 |                      |                    |
| python-pid                     | 3          | 3.0.4                |                  |                 |                      |                    |
| python-pip                     | 3          | 22.0.4               |                  |                 |                      |                    |
| python-platformdirs            | 3          | 2.5.2                |                  |                 |                      |                    |
| python-ply                     | 3          | 3.11                 |                  |                 |                      |                    |
| python-prettytable             | 3          | 3.2.0                |                  |                 |                      |                    |
| python-productmd               | 3          | 1.33                 |                  |                 |                      |                    |
| python-progressbar2            | 3          | 3.55.0               |                  |                 |                      |                    |
| python-pycdio                  | 3          | 2.1.1                |                  |                 |                      |                    |
| python-pycparser               | 3          | 2.21                 |                  |                 |                      |                    |
| python-pycurl                  | 3          | 7.45.1               |                  |                 |                      |                    |
| python-pyqt5-sip               | 3          | 12.11.0              | 是               | Meredith        |                      |                    |
| python-pyrsistent              | 3          | 0.18.1               |                  |                 |                      |                    |
| python-pyserial                | 3          | 3.5                  |                  |                 |                      |                    |
| python-pysocks                 | 3          | 1.7.1                |                  |                 |                      |                    |
| python-pytoml                  | 3          | 0.1.18               |                  |                 |                      |                    |
| python-pyudev                  | 3          | 0.23.2               |                  |                 |                      |                    |
| python-qt5                     | 3          | 5.15.6               |                  |                 |                      |                    |
| python-requests                | 3          | 2.28.1               |                  |                 |                      |                    |
| python-requests-file           | 3          | 1.5.1                |                  |                 |                      |                    |
| python-requests-ftp            | 3          | 0.3.1                |                  |                 |                      |                    |
| python-rpm-generators          | 3          | 12                   |                  |                 |                      |                    |
| python-rpm-macros              | 3          | 3.10                 |                  |                 |                      |                    |
| python-ruamel-yaml             | 3          | 0.17.21              |                  |                 |                      |                    |
| python-ruamel-yaml-clib        | 3          | 0.2.6                |                  |                 |                      |                    |
| python-schedutils              | 3          | 0.6                  |                  |                 |                      |                    |
| python-setuptools              | 3          | 60.9.3               |                  |                 |                      |                    |
| python-simpleline              | 3          | 1.9.0                |                  |                 |                      |                    |
| python-six                     | 3          | 1.16.0               |                  |                 |                      |                    |
| python-slip                    | 3          | 0.6.5                |                  |                 |                      |                    |
| python-sphinx                  | 3          | 5.1.1                |                  |                 |                      |                    |
| python-systemd                 | 3          | 235                  |                  |                 |                      |                    |
| python-tornado                 | 3          | 6.1.0                |                  |                 |                      |                    |
| python-urllib3                 | 3          | 1.26.9               |                  |                 |                      |                    |
| python-utils                   | 3          | 3.1.0                |                  |                 |                      |                    |
| python-virtualenv              | 3          | 20.16.5              |                  |                 |                      |                    |
| python-wcwidth                 | 3          | 0.2.5                |                  |                 |                      |                    |
| python-wheel                   | 3          | 0.37.1               |                  |                 |                      |                    |
| PyYAML                         | 3          | 6.0                  |                  |                 |                      |                    |
| qatengine                      | 3          | 0.6.16               | 是               | happy_orange    |                      |                    |
| qatlib                         | 3          | 22.07.0              | 是               | happy_orange    |                      |                    |
| qatzip                         | 3          | 1.0.9                | 是               | happy_orange    |                      |                    |
| qemu                           | 3          | 6.2.0                |                  | xuchun-shang    |                      |                    |
| qpl                            | 3          | 0.2.0                | 是               | happy_orange    |                      |                    |
| qpress                         | 3          | 11                   |                  |                 |                      |                    |
| qrencode                       | 3          | 4.1.1                |                  |                 |                      |                    |
| qt5                            | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtbase                     | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtconnectivity             | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtdeclarative              | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtlocation                 | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtmultimedia               | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtsensors                  | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtserialport               | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtsvg                      | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qttools                    | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtwebchannel               | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtwebkit                   | 3          | 5.212.0              |                  |                 |                      |                    |
| qt5-qtwebsockets               | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtx11extras                | 3          | 5.15.5               |                  |                 |                      |                    |
| qt5-qtxmlpatterns              | 3          | 5.15.5               |                  |                 |                      |                    |
| quota                          | 3          | 4.06                 |                  |                 |                      |                    |
| rdma-core                      | 3          | 39.0                 |                  |                 |                      |                    |
| realmd                         | 3          | 0.17.0               |                  |                 |                      |                    |
| rootfiles                      | 3          | 23                   |                  |                 |                      |                    |
| rpmdevtools                    | 3          | 9.6                  |                  |                 |                      |                    |
| rpm-ostree                     | 3          | 2022.2               |                  |                 |                      |                    |
| rsync                          | 3          | 3.2.5                | 是               | 扣肉酱          |                      |                    |
| rtkit                          | 3          | 0.11                 |                  |                 |                      |                    |
| ruby                           | 3          | 3.1.2                |                  |                 |                      |                    |
| rust-packaging                 | 3          | 21                   |                  |                 |                      |                    |
| rust-srpm-macros               | 3          | 23                   |                  |                 |                      |                    |
| rust-toolset                   | 3          | 1.59.0               |                  |                 |                      |                    |
| rust-zram-generator            | 3          | 1.1.2                |                  |                 |                      |                    |
| samba                          | 3          | 4.16.1               |                  |                 |                      |                    |
| satyr                          | 3          | 0.39                 |                  |                 |                      |                    |
| sbc                            | 3          | 1.5                  |                  |                 |                      |                    |
| SDL2                           | 3          | 2.24.0               |                  |                 |                      |                    |
| SDL2_image                     | 3          | 2.6.1                |                  |                 |                      |                    |
| seal                           | 3          | 3.7.2                |                  |                 |                      |                    |
| sed                            | 3          | 4.9                  | 是               | 扣肉酱          |                      |                    |
| setools                        | 3          | 4.4.0                |                  |                 |                      |                    |
| setxkbmap                      | 3          | 1.3.3                |                  |                 |                      |                    |
| sg3_utils                      | 3          | 1.47                 |                  |                 |                      |                    |
| sgml-common                    | 3          | 0.6.3                |                  |                 |                      |                    |
| sgpio                          | 3          | 1.2.1                |                  |                 |                      |                    |
| shared-mime-info               | 3          | 2.2                  |                  |                 |                      |                    |
| shim-unsigned-aarch64          | 3          | 15.4                 |                  |                 |                      |                    |
| shim-unsigned-x64              | 3          | 15.4                 |                  |                 |                      |                    |
| sil-padauk-fonts               | 3          | 5.000                |                  |                 |                      |                    |
| slang                          | 3          | 2.3.2                |                  |                 |                      |                    |
| smartmontools                  | 3          | 7.3                  |                  |                 |                      |                    |
| snappy                         | 3          | 1.1.9                |                  |                 |                      |                    |
| socat                          | 3          | 1.7.4.2              |                  |                 |                      |                    |
| sombok                         | 3          | 2.4.0                |                  |                 |                      |                    |
| sound-theme-freedesktop        | 3          | 0.8                  |                  |                 |                      |                    |
| soundtouch                     | 3          | 2.3.1                |                  |                 |                      |                    |
| source-highlight               | 3          | 3.1.9                |                  |                 |                      |                    |
| speex                          | 3          | 1.2.0                |                  |                 |                      |                    |
| speexdsp                       | 3          | 1.2.0                |                  |                 |                      |                    |
| spice                          | 3          | 0.15.0               |                  |                 |                      |                    |
| spice-vdagent                  | 3          | 0.22.1               |                  |                 |                      |                    |
| squashfs-tools                 | 3          | 4.5                  |                  |                 |                      |                    |
| ssar                           | 3          | 1.0.3                | 是               | happy_orange    | 是                   |                    |
| sscg                           | 3          | 3.0.2                |                  |                 |                      |                    |
| star                           | 3          | 1.6                  |                  |                 |                      |                    |
| startup-notification           | 3          | 0.12                 |                  |                 |                      |                    |
| strace                         | 3          | 5.16                 |                  |                 |                      |                    |
| supervisor                     | 3          | 4.2.4                |                  |                 |                      |                    |
| sysak-5.10.134-12.1            | 3          | 1.3.0                | 是               | happy_orange    | 是                   |                    |
| sysfsutils                     | 3          | 2.1.1                |                  |                 |                      |                    |
| sysom                          | 3          | 2.0                  | 是               | happy_orange    | 是                   |                    |
| sysprof                        | 3          | 3.44.0               |                  |                 |                      |                    |
| sysstat                        | 3          | 12.5.5               |                  |                 |                      |                    |
| systemtap                      | 3          | 4.6                  |                  |                 |                      |                    |
| taglib                         | 3          | 1.12                 |                  |                 |                      |                    |
| tbb                            | 3          | 2020.3               |                  |                 |                      |                    |
| texinfo                        | 3          | 6.8                  | 是               | 扣肉酱          |                      |                    |
| texlive                        | 3          | 2021                 |                  |                 |                      |                    |
| texlive-base                   | 3          | 20210325             |                  |                 |                      |                    |
| thai-scalable-fonts            | 3          | 0.7.3                |                  |                 |                      |                    |
| tigervnc                       | 3          | 1.12.0               |                  |                 |                      |                    |
| timedatex                      | 3          | 0.6                  |                  |                 |                      |                    |
| tk                             | 3          | 8.6.12               |                  |                 |                      |                    |
| tmux                           | 3          | 3.2a                 |                  |                 |                      |                    |
| totem-pl-parser                | 3          | 3.26.6               |                  |                 |                      |                    |
| tpm2-tss                       | 3          | 3.2.0                |                  |                 |                      |                    |
| tracker                        | 3          | 3.3.0                |                  |                 |                      |                    |
| tracker-miners                 | 3          | 3.3.0                |                  |                 |                      |                    |
| trousers                       | 3          | 0.3.15               |                  |                 |                      |                    |
| ttmkfdir                       | 3          | 3.0.9                |                  |                 |                      |                    |
| twolame                        | 3          | 0.4.0                |                  |                 |                      |                    |
| tzdata                         | 3          | 2022a                | 是               | 扣肉酱          |                      |                    |
| uboot-tools                    | 3          | 2022.04              |                  |                 |                      |                    |
| uchardet                       | 3          | 0.0.7                |                  |                 |                      |                    |
| ucs-miscfixed-fonts            | 3          | 0.3                  |                  |                 |                      |                    |
| udisks2                        | 3          | 2.9.4                |                  |                 |                      |                    |
| unbound                        | 3          | 1.15.0               |                  |                 |                      |                    |
| unixODBC                       | 3          | 2.3.11               |                  |                 |                      |                    |
| upower                         | 3          | 0.99.17              |                  |                 |                      |                    |
| usbutils                       | 3          | 014                  |                  |                 |                      |                    |
| userspace-rcu                  | 3          | 0.13.0               |                  |                 |                      |                    |
| v4l-utils                      | 3          | 1.22.1               |                  |                 |                      |                    |
| vim                            | 3          | 9.0                  |                  |                 |                      |                    |
| virt-what                      | 3          | 1.21                 |                  |                 |                      |                    |
| volume_key                     | 3          | 0.3.12               |                  |                 |                      |                    |
| vtable-dumper                  | 3          | 1.2                  |                  |                 |                      |                    |
| vte291                         | 3          | 0.68.0               |                  |                 |                      |                    |
| vulkan-headers                 | 3          | 1.3.234              | 是               | 扣肉酱          |                      |                    |
| vulkan-loader                  | 3          | 1.3.211              | 是               | 扣肉酱          |                      |                    |
| wavpack                        | 3          | 5.4.0                |                  |                 |                      |                    |
| wayland                        | 3          | 1.21.0               |                  |                 |                      |                    |
| web-assets                     | 3          | 23                   |                  |                 |                      |                    |
| webkit2gtk3                    | 3          | 2.37.1               | 是               | 扣肉酱          |                      |                    |
| webrtc-audio-processing        | 3          | 0.3.1                |                  |                 |                      |                    |
| wget                           | 3          | 1.21.2               | 是               | 扣肉酱          |                      |                    |
| whois                          | 3          | 5.5.12               |                  |                 |                      |                    |
| wireless-regdb                 | 3          | 2022.04.08           |                  |                 |                      |                    |
| wireplumber                    | 3          | 0.4.9                |                  |                 |                      |                    |
| woff2                          | 3          | 1.0.2                |                  |                 |                      |                    |
| words                          | 3          | 3.0                  |                  |                 |                      |                    |
| wpa_supplicant                 | 3          | 2.10                 |                  |                 |                      |                    |
| wpebackend-fdo                 | 3          | 1.14.0               |                  |                 |                      |                    |
| xcb-util                       | 3          | 0.4.0                |                  |                 |                      |                    |
| xcb-util-image                 | 3          | 0.4.0                | 是               | 扣肉酱          |                      |                    |
| xcb-util-keysyms               | 3          | 0.4.0                | 是               | 扣肉酱          |                      |                    |
| xcb-util-renderutil            | 3          | 0.3.10               | 是               | 扣肉酱          |                      |                    |
| xcb-util-wm                    | 3          | 0.4.2                | 是               | 扣肉酱          |                      |                    |
| xdg-dbus-proxy                 | 3          | 0.1.3                |                  |                 |                      |                    |
| xfsdump                        | 3          | 3.1.10               |                  | taijuanZZY      |                      |                    |
| xhost                          | 3          | 1.0.8                |                  |                 |                      |                    |
| xkbcomp                        | 3          | 1.4.5                |                  |                 |                      |                    |
| xkeyboard-config               | 3          | 2.35.1               |                  |                 |                      |                    |
| xmlrpc-c                       | 3          | 1.54.05              |                  |                 |                      |                    |
| xmodmap                        | 3          | 1.0.11               |                  |                 |                      |                    |
| xorg-x11-drivers               | 3          | 23                   |                  |                 |                      |                    |
| xorg-x11-drv-ati               | 3          | 19.1.0               |                  |                 |                      |                    |
| xorg-x11-drv-dummy             | 3          | 0.4.0                |                  |                 |                      |                    |
| xorg-x11-drv-evdev             | 3          | 2.10.6               |                  |                 |                      |                    |
| xorg-x11-drv-fbdev             | 3          | 0.5.0                |                  |                 |                      |                    |
| xorg-x11-drv-intel             | 3          | 2.99.917             |                  |                 |                      |                    |
| xorg-x11-drv-libinput          | 3          | 1.2.1                |                  |                 |                      |                    |
| xorg-x11-drv-nouveau           | 3          | 1.0.17               |                  |                 |                      |                    |
| xorg-x11-drv-openchrome        | 3          | 0.6.400              |                  |                 |                      |                    |
| xorg-x11-drv-qxl               | 3          | 0.1.5                |                  |                 |                      |                    |
| xorg-x11-drv-vesa              | 3          | 2.5.0                |                  |                 |                      |                    |
| xorg-x11-drv-vmware            | 3          | 13.3.0               |                  |                 |                      |                    |
| xorg-x11-drv-wacom             | 3          | 1.0.0                |                  |                 |                      |                    |
| xorg-x11-fonts                 | 3          | 7.5                  |                  |                 |                      |                    |
| xorg-x11-proto-devel           | 3          | 2022.1               |                  |                 |                      |                    |
| xorg-x11-server                | 3          | 1.20.14              |                  |                 |                      |                    |
| xorg-x11-xauth                 | 3          | 1.1.2                |                  |                 |                      |                    |
| xorg-x11-xinit                 | 3          | 1.4.0                |                  |                 |                      |                    |
| xrandr                         | 3          | 1.5.1                |                  |                 |                      |                    |
| xrdb                           | 3          | 1.2.1                |                  |                 |                      |                    |
| xterm                          | 3          | 372                  |                  |                 |                      |                    |
| xxhash                         | 3          | 0.8.1                |                  |                 |                      |                    |
| xz                             | 3          | 5.2.6                | 是               | 扣肉酱          |                      |                    |
| yelp                           | 3          | 42.1                 |                  |                 |                      |                    |
| yelp-xsl                       | 3          | 42.0                 | 是               | 扣肉酱          |                      |                    |
| zchunk                         | 3          | 1.2.1                |                  |                 |                      |                    |
| zenity                         | 3          | 3.42.0               |                  |                 |                      |                    |
| zip                            | 3          | 3.0                  |                  |                 |                      |                    |
| xcb-proto                      | 3          | 1.15.2               | 是               | 扣肉酱          |                      |                    |
| containerd                     | 4          | 1.5.9                |                  |                 |                      |                    |
| containernetworking-plugins    | 4          | 1.1.1                |                  |                 |                      |                    |
| container-selinux              | 4          | 2.189.0              |                  |                 |                      |                    |
| cri-tools                      | 4          | 1.23.0               |                  |                 |                      |                    |
| docker                         | 4          | 20.10.16             | 是               | Meredith        |                      |                    |
| httpd                          | 4          | 2.4.54               |                  |                 |                      |                    |
| kubernetes                     | 4          | 1.23.3               |                  |                 |                      |                    |
| mariadb                        | 4          | 10.6.8               |                  | taijuanZZY      | 是                   |                    |
| mariadb-connector-c            | 4          | 3.2.7                |                  |                 |                      |                    |
| memcached                      | 4          | 1.6.16               |                  |                 |                      |                    |
| mysql                          | 4          | 8.0.28               |                  | taijuanZZY      |                      |                    |
| mysql-selinux                  | 4          | 1.0.5                |                  |                 |                      |                    |
| nginx                          | 4          | 1.23.1               |                  | taijuanZZY      |                      |                    |
| nodejs                         | 4          | 16.17.0              |                  |                 |                      |                    |
| percona-toolkit                | 4          | 3.4.0                | 是               | Meredith        |                      |                    |
| percona-xtrabackup-24          | 4          | 2.4.26               | 是               | Meredith        |                      |                    |
| percona-xtrabackup-80          | 4          | 8.0.28               | 是               | Meredith        |                      |                    |
| percona-xtradb-cluster         | 4          | 8.0.28               | 是               | Meredith        |                      |                    |
| Percona-XtraDB-Cluster-57      | 4          | 5.7.39               | 是               | Meredith        |                      |                    |
| php                            | 4          | 8.1.9                | 是               | 扣肉酱          |                      |                    |
| redis                          | 4          | 7.0.4                |                  | taijuanZZY      | 是                   |                    |
| runc                           | 4          | 1.1.3                |                  |                 |                      |                    |
| tomcat                         | 4          | 9.0.65               |                  |                 |                      |                    |
