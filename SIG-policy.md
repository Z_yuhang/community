# OpenAnolis SIG流程

SIG(Special Interest Groups)是OpenAnolis社区里的兴趣小组，围绕Anolis OS某一特定主题进行开发。任何对某一领域及技术感兴趣的人都可以发起、管理或者贡献社区SIG。

# 社区SIG流程

## 1、SIG申请填写

提交申请PR到TC代码库：https://codeup.openanolis.org/codeup/community/tc 文档包含以下信息：

•小组名称

•小组介绍及目标

•项目列表及repo

•路线图及工作计划

•邮件列表及会议等沟通信息

•成员及维护人（使用网站账号ID）

•项目文档

## 2、申请提交

发送邮件到技术委员会tc@lists.openanolis.org提交创建申请，并进入TC委员会讨论议题。

## 3、技术委员会审批

•社区技术委员会将在社区会议上对SIG申请进行审批，对SIG申请PR给出反馈，进行PR合并或拒绝。

•技术委员会指派一位委员帮助SIG的建立，包括协助相关资源，指导SIG的建立和初期运转。

## 4、SIG创建

•填充 SIG页面 [https](https://openanolis.org/sig)[://openanolis](https://openanolis.org/sig)[.org/sig](https://openanolis.org/sig)

•创建SIG所含项目的代码仓库

•申请访问Buildsystem来构建rpms软件包

•申请访问QA测试系统来测试软件包

•申请SIG的Bugs管理权限

•申请SIG邮件列表

## 5、SIG运行和管理

•各SIG实行自治管理，按照多数票原则投票决策。

•各SIG每个半年向技术委员会做运行总结报告，TC给出相应的指导和帮助。

•其他合并等变更有SIG 维护者向技术委员会提交申请，并获批后变更。

## 6、SIG退出机制

TC委员会可依据以下原则，由技术委员提出并提交TC委员会会议决议SIG的撤销：

•SIG的工作内容无法满足社区发行版或者社区技术发展方向。

•SIG长期没有活跃度，或者无成果产出。